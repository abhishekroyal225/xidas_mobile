import 'dart:convert';
import 'package:ZettaWifiDirect/model/measurement_rate_model.dart';
import 'package:ZettaWifiDirect/model/transmit_rate_model.dart';
import 'package:flutter/cupertino.dart';
import '../model/echip_model.dart';
import 'package:http/http.dart' as http;
import '../model/devices_model.dart';

class Services {
  static const String getDevicesURL = 'https://34.213.13.191:8443/get-devices';
  static const String getEchipDetailsURL =
      'https://34.213.13.191:8443/view-host-echip'; //dummy
  static const String getEchipsByDeviceIdURL =
      'https://34.213.13.191:8443/view-device-echip-by-id?deviceid=';

  static const String measurementRateUrl =
      'https://34.213.13.191:8443/get-measurement-rates';

  static const String transmitRateUrl =
      'https://34.213.13.191:8443/get-transmit-rates';

  static Future<List<DevicesModel>> getDevices() async {
    try {
      final response = await http.get(Uri.parse(getDevicesURL));
      if (response.statusCode == 200) {
        List<DevicesModel> devicesModelList = parseDevicesList(response.body);
        return devicesModelList;
      } else {
        throw Exception('Error');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static List<DevicesModel> parseDevicesList(String responseBody) {
    final parsedDevices = json.decode(responseBody);
    return parsedDevices
        .map<DevicesModel>((json) => DevicesModel.fromJson(json))
        .toList();
  }

  static Future<DevicesModel> getEchipsByDeviceID(int deviceId) async {
    final url = "$getEchipsByDeviceIdURL$deviceId";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode != 200)
      throw Exception('${response.statusCode}: ${response.body}');
    final bodyJson = json.decode(response.body);
    print(bodyJson);
    final data = DevicesModel.fromJson(bodyJson.first);
    return data;
  }

  static Future saveEchipDetails(DevicesModel device) async {
    final devJson = device.toJson();
    final encodedJson = json.encode(devJson);
    //send this to the API now
    print(encodedJson);
  }

  static Future<List<MeasurementRate>> getMeasurementRate() async {
    final response = await http.get(Uri.parse(measurementRateUrl));

    if (response.statusCode != 200)
      throw Exception('${response.statusCode}: ${response.body}');
    final bodyJson = json.decode(response.body);
    return bodyJson
        .map<MeasurementRate>((json) => MeasurementRate.fromJson(json))
        .toList();
  }

  static Future<List<TransmitRate>> getTransmitRate() async {
    final response = await http.get(Uri.parse(transmitRateUrl));

    if (response.statusCode != 200)
      throw Exception('${response.statusCode}: ${response.body}');
    final bodyJson = json.decode(response.body);
    return bodyJson
        .map<TransmitRate>((json) => TransmitRate.fromJson(json))
        .toList();
  }
}
