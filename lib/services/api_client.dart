import 'package:ZettaWifiDirect/model/login_response.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
part 'api_client.g.dart';

@RestApi(baseUrl: 'http://34.213.13.191:8080/')
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) {
    dio.options = BaseOptions(
        receiveTimeout: 5000,
        connectTimeout: 5000,
        contentType: 'application/json',
        headers: {'contentType': 'application/json'});
    return _ApiClient(dio, baseUrl: baseUrl);
  }

  @POST('/user-login') // enter your api method
  Future<LoginResponse> login(@Body() Map<String, dynamic> body);
}
