import 'package:flutter/cupertino.dart';

class AuthService with ChangeNotifier {
  String email;

  void setCredentials({String email}) {
    this.email = email;
    notifyListeners();
  }
}
