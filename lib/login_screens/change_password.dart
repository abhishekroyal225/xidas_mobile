import 'package:ZettaWifiDirect/main_screens/Login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChangePassword extends StatefulWidget {
  final String title;
  ChangePassword({this.title});

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  double _height, _width;
  TextEditingController _currentPasswordController = TextEditingController();
  TextEditingController _newPasswordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();
  bool _fieldError = false;
  bool _isCurrentPasswordFilled = false;
  bool _isNewPasswordFilled = false;
  bool _isConfirmPasswordFilled = false;
  bool _isCurrentPaswordHidden = true;
  bool _isNewPasswordHidden = true;
  bool _isConfirmPasswordHidden = true;

  String _strCurrentPassword = '',
      _strNewPassword = '',
      _strConfirmPassword = '';

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Container(
        margin: EdgeInsets.only(
          top: _height * 0.04,
        ),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(10),
                child: Text(
                  'Change Password',
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 30,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                  obscureText: _isCurrentPaswordHidden,
                  controller: _currentPasswordController,
                  decoration: InputDecoration(
                    icon: Icon(Icons.lock),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(2.0),
                    ),
                    labelText: 'Current Password',
                    hintText: 'Enter Current Password',
                    errorText: _fieldError && !_isCurrentPasswordFilled
                        ? 'Current Password Cannot Be Empty'
                        : null,
                    suffix: InkWell(
                      onTap: _toggleCurrentPasswordView,
                      child: Icon(
                        _isCurrentPaswordHidden
                            ? Icons.visibility_off
                            : Icons.visibility,
                      ),
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {
                      if (value == '' || value == null) {
                        _isCurrentPasswordFilled = false;
                      } else {
                        _isCurrentPasswordFilled = true;
                      }
                    });
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(10, 8, 10, 0),
                child: TextField(
                  obscureText: _isNewPasswordHidden,
                  controller: _newPasswordController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(2.0),
                    ),
                    icon: Icon(Icons.lock),
                    labelText: 'New Password',
                    hintText: 'Enter New Password',
                    errorText: _fieldError && !_isNewPasswordFilled
                        ? 'New Password Cannot Be Empty'
                        : null,
                    suffix: InkWell(
                      onTap: _toggleNewPasswordView,
                      child: Icon(
                        _isNewPasswordHidden
                            ? Icons.visibility_off
                            : Icons.visibility,
                      ),
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {
                      if (value == '' || value == null) {
                        _isNewPasswordFilled = false;
                      } else {
                        _isNewPasswordFilled = true;
                      }
                    });
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(10, 16, 10, 0),
                child: TextField(
                  obscureText: _isConfirmPasswordHidden,
                  controller: _confirmPasswordController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(2.0),
                    ),
                    icon: Icon(Icons.lock),
                    labelText: 'Confirm Password',
                    hintText: 'Re Enter Password',
                    errorText: _fieldError && !_isConfirmPasswordFilled
                        ? 'Confirm Password Cannot Be Empty'
                        : null,
                    suffix: InkWell(
                      onTap: _toggleConfirmPasswordView,
                      child: Icon(
                        _isConfirmPasswordHidden
                            ? Icons.visibility_off
                            : Icons.visibility,
                      ),
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {
                      if (value == '' || value == null) {
                        _isConfirmPasswordFilled = false;
                      } else {
                        _isConfirmPasswordFilled = true;
                      }
                    });
                  },
                ),
              ),
              Container(
                height: 50,
                margin: EdgeInsets.only(top: _height * 0.02),
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                child: RaisedButton(
                  textColor: Colors.white,
                  color: Colors.blue,
                  child: Text(
                    'CHANGE PASSWORD',
                    style: TextStyle(fontSize: 18),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  onPressed: () {
                    if (_isCurrentPasswordFilled &&
                        _isNewPasswordFilled &&
                        _isConfirmPasswordFilled) {
                      print(_currentPasswordController.text);
                      print(_newPasswordController.text);
                      print(_confirmPasswordController.text);
                      _strCurrentPassword =
                          _currentPasswordController.text.trim();
                      _strNewPassword = _newPasswordController.text.trim();
                      _strConfirmPassword =
                          _confirmPasswordController.text.trim();
                      setState(() {
                        _fieldError = false;
                      });
                      if (_strNewPassword != _strConfirmPassword) {
                        // warningDialog(context, 'Passwords Does Not Match');
                        Container();
                      } else {
                        // successDialog(context, 'Changed Successfully',
                        //     neutralAction: _goToLogin);
                        Container();
                      }
                    } else {
                      setState(() {
                        _fieldError = true;
                      });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _goToLogin() {
    Navigator.pushAndRemoveUntil(
        context,
        CupertinoPageRoute(
          builder: (context) => Login(title: 'XIDAS IOT Mobile App'),
        ),
        (route) => true);
  }

  void _toggleCurrentPasswordView() {
    setState(() {
      _isCurrentPaswordHidden = !_isCurrentPaswordHidden;
    });
  }

  void _toggleNewPasswordView() {
    setState(() {
      _isNewPasswordHidden = !_isNewPasswordHidden;
    });
  }

  void _toggleConfirmPasswordView() {
    setState(() {
      _isConfirmPasswordHidden = !_isConfirmPasswordHidden;
    });
  }

  snackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(text),
        duration: Duration(seconds: 2),
      ),
    );
  }
}
