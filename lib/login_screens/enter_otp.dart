import 'package:ZettaWifiDirect/main_screens/WiFiDevices.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otp_screen/otp_screen.dart';

class EnterOTP extends StatefulWidget {
  final String title;
  EnterOTP({this.title});

  @override
  _EnterOTPState createState() => _EnterOTPState();
}

class _EnterOTPState extends State<EnterOTP> {
  double _height, _width;
  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: OtpScreen.withGradientBackground(
          topColor: Color(0xFFcc2b5e),
          bottomColor: Color(0xFF753a88),
          otpLength: 4,
          validateOtp: validateOtp,
          routeCallback: moveToNextScreen,
          themeColor: Colors.white,
          titleColor: Colors.white,
          title: 'Phone Number Verification',
          subTitle: 'Enter The Code Sent to \n phone or email',
          icon: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.phone_android_outlined,
              color: Colors.white,
            ),
          )),
    );
  }

  Future<String> validateOtp(String otp) async {
    await Future.delayed(Duration(milliseconds: 2000));
    if (otp == "1234") {
      return null;
    } else {
      return "The entered Otp is wrong";
    }
  }

  void moveToNextScreen(context) {
    Navigator.push(
        context,
        CupertinoPageRoute(
            builder: (context) => WiFiDevices(
                  title: 'XIDAS IOT Mobile App',
                )));
  }
}
