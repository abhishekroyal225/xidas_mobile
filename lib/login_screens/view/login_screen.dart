import 'package:ZettaWifiDirect/dashboard_screens/dashboard.dart';
import 'package:ZettaWifiDirect/services/api_client.dart';
import 'package:ZettaWifiDirect/services/base_model.dart';
import 'package:ZettaWifiDirect/services/server_error.dart';
import 'package:ZettaWifiDirect/util/progressDialogBox.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:dio/dio.dart' as dio;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  var _emailOffset = 0.0;
  var _emailColor = 0xFFE1E1E1;
  var _emailSupportText = true;

  var _passwordOffset = 0.0;
  var _passwordColor = 0xFFE1E1E1;

  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    //loginBloc = context.dfd
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: const Text('Login')),
      body: SafeArea(
        // alignment: const Alignment(-1 / 1.1, -1 / 1.1),
        child: SingleChildScrollView(
            child: Column(children: <Widget>[
          Container(
              child: Row(
            children: [
              SizedBox(
                width: 30.w,
                height: 150.h,
              ),
              Image.asset(
                'assets/images/logo.png',
                height: 65.h,
                width: 150.w,
              ),
            ],
          )),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 32.w),
            child: Column(
              children: [
                Container(
                    child: Column(
                  children: [
                    const SizedBox(height: 40),
                    Container(
                      child: Text(
                        'Log in',
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 36,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      //padding: EdgeInsets.only(top: _height * 0.01),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Don't have an account?",
                            style: TextStyle(
                              color: Color(0xffb6b6b6),
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(
                            "  Sign up",
                            style: TextStyle(
                              color: Color(0xFF08F0A9),
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 50),
                    Container(
                      height: 65.h,
                      decoration: BoxDecoration(
                        color: Color(0xFFFFFFFF),
                        border: Border.all(
                          color: Color(0xFFE1E1E1),
                        ),
                        borderRadius: BorderRadius.circular(10.r),
                        boxShadow: [
                          BoxShadow(
                              color: Color(_emailColor),
                              offset: Offset(_emailOffset, 0),
                              spreadRadius: -1),
                        ],
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(8.0, 5, 3.0, 0),
                        child: Column(
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              FocusScope(
                                child: Focus(
                                  onFocusChange: (focus) => setState(() {
                                    if (focus) {
                                      _emailOffset = -5.0;
                                      _emailColor = 0xFF08F0A9;
                                    } else {
                                      _emailOffset = 0.0;
                                      _emailColor = 0xFFE1E1E1;
                                    }
                                  }),
                                  child: Container(
                                    height: 45,
                                    width: double.infinity,
                                    child: TextField(
                                      controller: emailController,
                                      key: const Key(
                                          'loginForm_emailInput_textField'),
                                      textInputAction: TextInputAction.next,
                                      keyboardType: TextInputType.emailAddress,
                                      focusNode: _emailFocus,
                                      decoration: InputDecoration(
                                          labelText: 'Email',
                                          labelStyle: TextStyle(
                                              color: Color(0xFF838383)),
                                          border: UnderlineInputBorder(),
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          contentPadding:
                                              EdgeInsets.only(top: -8),
                                          prefixIcon: Icon(Icons.email,
                                              color: Color(0xFF838383)),
                                          hintText: 'Email'),
                                      onEditingComplete: () =>
                                          _fieldFocusChange(context,
                                              _emailFocus, _passwordFocus),
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                      ),
                    ),
                    const SizedBox(height: 20),
                    Container(
                      height: 65.h,
                      decoration: BoxDecoration(
                        color: Color(0xFFFFFFFF),
                        border: Border.all(
                          color: Color(0xFFE1E1E1),
                        ),
                        borderRadius: BorderRadius.circular(10.r),
                        boxShadow: [
                          BoxShadow(
                              color: Color(_passwordColor),
                              offset: Offset(_passwordOffset, 0),
                              spreadRadius: -1),
                        ],
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(8.0, 5, 3.0, 0),
                        child: Column(
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              FocusScope(
                                child: Focus(
                                  onFocusChange: (focus) => setState(() {
                                    if (focus) {
                                      _passwordOffset = -5.0;
                                      _passwordColor = 0xFF08F0A9;
                                    } else {
                                      _passwordOffset = 0.0;
                                      _passwordColor = 0xFFE1E1E1;
                                    }
                                  }),
                                  child: Container(
                                    height: 45,
                                    width: double.infinity,
                                    child: TextField(
                                      controller: passwordController,
                                      key: const Key(
                                          'loginForm_passwordInput_textField'),
                                      textInputAction: TextInputAction.done,
                                      keyboardType: TextInputType.emailAddress,
                                      focusNode: _passwordFocus,
                                      decoration: InputDecoration(
                                          labelText: 'Password',
                                          labelStyle: TextStyle(
                                              color: Color(0xFF838383)),
                                          border: UnderlineInputBorder(),
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          contentPadding:
                                              EdgeInsets.only(top: -8),
                                          prefixIcon: Icon(Icons.lock,
                                              color: Color(0xFF838383)),
                                          suffixIcon: Icon(Icons.visibility,
                                              color: Color(0xFF838383)),
                                          hintText: 'Password'),
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        TextButton(
                          child: Text(
                            'Forgot Password?',
                            style:
                                Theme.of(context).textTheme.bodyText2.copyWith(
                                      color: Color(0xff828282),
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                          onPressed: () {
                            //forgot password screen
                          },
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    SizedBox(
                      height: 60,
                      width: 320,
                      child: ElevatedButton(
                        onPressed: () {
                          _checkLoginCrediential(
                              context,
                              emailController.text.trim(),
                              passwordController.text.trim());
                        },
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0))),
                          backgroundColor:
                              MaterialStateProperty.all(Colors.black),
                        ),
                        child: Text(
                          'Sign In',
                          style: TextStyle(
                            fontSize: 26.sp,
                            // fontWeight: FontWeight.w200,
                            fontFamily: 'poppins',
                          ),
                        ),
                        //  icon: Icon(Icons.person_add_alt_1_rounded, size: 18),
                        //label: Text("Sign in"),
                      ),
                    ),
                    const SizedBox(height: 160),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Image.asset(
                        'assets/images/xidaslogo.png',
                        height: 65.h,
                        width: 150.w,
                      ),
                    )
                  ],
                ))
              ],
            ),
          ),
        ])),
      ),
    );
  }
}

_fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}

_checkLoginCrediential(
    BuildContext context, String email, String password) async {
  final Map<String, dynamic> logicData = {'email': email, 'password': password};
  ApiClient apiClient = ApiClient(dio.Dio());
  try {
    final response = await apiClient.login(logicData);
    print('${response.toJson()}');
    //ProgressDialogBox.showPopUpDialog(context, 'Alert', 'Login Success');
    Navigator.pushAndRemoveUntil(
        context,
        CupertinoPageRoute(
          builder: (context) => Dashboard(),
        ),
        (route) => true);
  } on DioError catch (error, stacktrace) {
    if (error is DioError) {
      //   print(error.response!.statusCode);
    }

    BaseModel baseModel = BaseModel()
      ..setException(ServerError.withError(error: error));
    ServerError serverError = baseModel.getException;

    ProgressDialogBox.showPopUpDialog(
        context, 'Alert', serverError.getErrorMessage());
    print("Exception occured: $error stackTrace: $stacktrace");
  }

  //Future<http.Response> response = APIRepository().login(email, password);
}
