import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class ProgressDialogBox {
  static void showCircularProgressIndicator(
      BuildContext context, String title) async {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            content: Container(
              width: 250.0,
              height: 120.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      title,
                      style: TextStyle(color: Color(0xFF5B6978)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.white,
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.blue),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  static showPopUpDialog(
      BuildContext context, String title, String content) async {
    await Alert(
        context: context,
        title: title,
        content: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
          return Text(content);
        }),
        buttons: [
          DialogButton(
              child: Center(
                child: Text(
                  'OK',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                  ),
                ),
              ),
              onPressed: () => Navigator.pop(context))
        ]).show();
  }

  static showUploadFirmwareAgainDialog(BuildContext context, String title,
      String content, Function cancelFunction, Function okFunction) async {
    await Alert(
        context: context,
        title: title,
        content: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
          return Text(content);
        }),
        buttons: [
          DialogButton(
            child: Center(
              child: Text(
                'CANCEL',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                ),
              ),
            ),
            onPressed: cancelFunction,
          ),
          DialogButton(
            child: Center(
              child: Text(
                'OK',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                ),
              ),
            ),
            onPressed: okFunction,
          ),
        ]).show();
  }

  static showPopUpDialogWithOK(BuildContext context, String title,
      String content, Function okFunction) async {
    await Alert(
        context: context,
        title: title,
        content: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
          return Text(content);
        }),
        buttons: [
          DialogButton(
            child: Center(
              child: Text(
                'OK',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                ),
              ),
            ),
            onPressed: okFunction,
          ),
        ]).show();
  }
}
