import 'dart:convert';

import 'package:ZettaWifiDirect/model/devices_model.dart';
import 'package:ZettaWifiDirect/model/measurement_rate_model.dart';
import 'package:ZettaWifiDirect/model/saved_sensor_data.dart';
import 'package:ZettaWifiDirect/model/transmit_rate_model.dart';
import 'package:ZettaWifiDirect/util/progressDialogBox.dart';

import '../model/sensor_model.dart';
import '../model/echip_model.dart';
import '../services/Services.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import '../colors/all_colors.dart' as all_Colors;

class SensorSetup extends StatefulWidget {
  final String title;
  final int deviceId;
  final bool isDataGotFromSavedData;
  final SavedSensorData sensorData;

  SensorSetup(
      {this.title,
      this.deviceId,
      @required this.isDataGotFromSavedData,
      @required this.sensorData});

  @override
  _SensorSetupState createState() => _SensorSetupState();
}

class _SensorSetupState extends State<SensorSetup> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  double _height, _width;
  SensorInfo sensor;
  Future<DevicesModel> deviceModel;
  Future<List<MeasurementRate>> measurementRates;
  Future<List<TransmitRate>> transmitRates;

  // Future<List<EChip>> _eChipDetails;

  @override
  void initState() {
    super.initState();
    deviceModel = Services.getEchipsByDeviceID(widget.deviceId).then((device) {
      if (widget.isDataGotFromSavedData) {
        var _eChipDetails = device.eChipDetails;
        widget.sensorData.echipDetails.forEach((e) {
          e.sensorDetails.forEach((s) {
            s.alarms.forEach((a) {
              _eChipDetails
                  .firstWhere((_e) => _e.id == e.echipId, orElse: () => null)
                  ?.sensorDetails
                  ?.firstWhere((_s) => _s.typeId == s.sensorTypeId,
                      orElse: () => null)
                  ?.alarms
                  ?.add(Alarm(
                    type: a.type == 'Critical'
                        ? AlarmType.Critical
                        : AlarmType.Non,
                    value: a.value,
                    min: a.min,
                    max: a.max,
                  ));
              print('Added, ${a.toJson()}');
            });
          });
        });
      }
      return device;
    });
    measurementRates = Services.getMeasurementRate();
    transmitRates = Services.getTransmitRate();
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: all_Colors.bodyBackgroundColor,
      body: Container(
        margin: EdgeInsets.only(top: _height * 0.08),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                left: _width * 0.06,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Sensor Set Up',
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                      color: Colors.redAccent,
                    ),
                  ),
                  ButtonTheme(
                    minWidth: 48,
                    child: Container(
                      margin: EdgeInsets.only(right: _width * 0.04),
                      child: RaisedButton(
                        onPressed: () async {
                          final _dev = await deviceModel;
                          Services.saveEchipDetails(_dev);
                          final sendingData = json.encode(_dev.toJson());
                          if (widget.isDataGotFromSavedData) {
                            ProgressDialogBox.showCircularProgressIndicator(
                                context, 'Updating. Please Wait..');
                            var updateData = await http.post(
                              Uri.parse(
                                  'https://34.213.13.191:8443/update-device-config-data'),
                              body: sendingData,
                            );

                            if (updateData.statusCode == 201) {
                              Navigator.of(context).pop();
                              ProgressDialogBox.showPopUpDialog(
                                  context, 'Update Status', 'Success');
                            } else {
                              Navigator.of(context).pop();
                              ProgressDialogBox.showPopUpDialog(
                                  context, 'Update Status', 'Falied');
                            }
                          } else {
                            ProgressDialogBox.showCircularProgressIndicator(
                                context, 'Saving. Please Wait..');
                            var createData = await http.post(
                                Uri.parse(
                                    'https://34.213.13.191:8443/create-device-config-data'),
                                body: sendingData);

                            if (createData.statusCode == 201) {
                              Navigator.of(context).pop();
                              ProgressDialogBox.showPopUpDialog(
                                  context, 'Saving Status', 'Success');
                            } else {
                              Navigator.of(context).pop();
                              ProgressDialogBox.showPopUpDialog(
                                  context, 'Saving Status', 'Failed');
                            }
                          }
                        },
                        child: Text('SAVE'),
                        color: Theme.of(context).primaryColor,
                        textColor:
                            Theme.of(context).primaryTextTheme.button.color,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: _width * 0.06,
                top: _height * 0.01,
              ),
              child: Text(
                'Alarms And Notifications',
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ),
            Expanded(
              child: FutureBuilder<DevicesModel>(
                  future: deviceModel,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting ||
                        snapshot.connectionState == ConnectionState.active)
                      return Center(child: CircularProgressIndicator());
                    if (snapshot.hasError) {
                      print(snapshot.error);
                      return Center(
                        child: Text('Something went wrong.'),
                      );
                    }
                    var _eChipDetails = snapshot.data.eChipDetails;
                    return SingleChildScrollView(
                      child: ListView.builder(
                          padding: EdgeInsets.zero,
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: _eChipDetails.length,
                          itemBuilder: (context, position) {
                            final sensors =
                                _eChipDetails[position].sensorDetails;
                            return _buildEchipEntry(
                                _eChipDetails, position, sensors);
                          }),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }

  Column _buildEchipEntry(
      List<EChip> _eChipDetails, int position, List<Sensor> sensors) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(
            left: _width * 0.06,
            top: 24,
          ),
          child: Text(
            _eChipDetails[position].name,
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        ListView.builder(
          padding: EdgeInsets.zero,
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: sensors.length,
          itemBuilder: (context, index) {
            final sensor = sensors[index];
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(
                    left: _width * 0.06,
                    top: 8,
                  ),
                  child: Row(
                    children: [
                      Text(
                        '${sensor.name}',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        left: _width * 0.03,
                      )),
                      DropdownButton(
                        value: sensor.selectedUnit,
                        onChanged: (value) => setState(() {
                          sensor.selectedUnit = value;
                          sensor.clamp();
                        }),
                        items: sensor.units.map((e) {
                          return DropdownMenuItem<String>(
                            value: e,
                            child: Text(e.toString()),
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    children: [
                      Text('MR: '),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                      ),
                      FutureBuilder<List<MeasurementRate>>(
                          future: measurementRates,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              final rates = snapshot.data;
                              sensor.measurementRate =
                                  sensor.measurementRate ?? rates.first;
                              return DropdownButton(
                                value: sensor.measurementRate ?? rates.first,
                                onChanged: (value) => setState(() {
                                  sensor.measurementRate = value;
                                }),
                                items: rates.map((e) {
                                  return DropdownMenuItem<MeasurementRate>(
                                    value: e,
                                    child: Text('${e.measurementRate} Mins'),
                                  );
                                }).toList(),
                              );
                            } else
                              return Container();
                          }),
                      Padding(
                        padding: EdgeInsets.only(left: 16),
                      ),
                      Text('TR: '),
                      Padding(
                        padding: EdgeInsets.only(left: 8),
                      ),
                      FutureBuilder<List<TransmitRate>>(
                          future: transmitRates,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              final rates = snapshot.data;
                              sensor.transmitRate =
                                  sensor.transmitRate ?? rates.first;
                              return DropdownButton(
                                value: sensor.transmitRate,
                                onChanged: (value) => setState(() {
                                  sensor.transmitRate = value;
                                }),
                                items: rates.map((e) {
                                  return DropdownMenuItem<TransmitRate>(
                                    value: e,
                                    child: Text('${e.transmitRate} Hrs'),
                                  );
                                }).toList(),
                              );
                            } else
                              return Container();
                          }),
                    ],
                  ),
                ),
                if (sensor.alarms.length < 2)
                  Card(
                    shape: RoundedRectangleBorder(
                      side: new BorderSide(color: Colors.blue, width: 2.0),
                      borderRadius: BorderRadius.circular(24.0),
                    ),
                    elevation: 3,
                    margin: EdgeInsets.only(
                      left: _width * 0.08,
                      top: _height * 0.02,
                      right: _width * 0.662,
                    ),
                    child: ListTile(
                      title: Text('Add Alarm',
                          style: TextStyle(
                            fontSize: 15,
                            decoration: TextDecoration.underline,
                          )),
                      dense: true,
                      onTap: () => addAlarm(sensor),
                    ),
                  ),
                for (final entry in sensors[index].alarms.asMap().entries)
                  _buildAlarmEntry(entry, sensor),
              ],
            );
          },
        ),
      ],
    );
  }

  Builder _buildAlarmEntry(MapEntry<int, Alarm> entry, Sensor sensor) {
    return Builder(
      builder: (context) {
        final alarm = entry.value;
        return Container(
          padding: EdgeInsets.symmetric(vertical: 4),
          child: Column(
            children: [
              RangeSlider(
                values: RangeValues(
                    alarm.min ?? sensor.rangeMin, alarm.max ?? sensor.rangeMax),
                onChanged: (values) {
                  setState(
                    () {
                      alarm.min = values.start;
                      alarm.max = values.end;
                      sensor.clamp();
                      alarm.minController.text = alarm.min.toStringAsFixed(1);
                      alarm.maxController.text = alarm.max.toStringAsFixed(1);
                    },
                  );
                },
                min: 0,
                max: 150,
                divisions: 1000,
                labels: RangeLabels(
                    '${(alarm.min ?? sensor.rangeMin).toStringAsFixed(1)} ${sensor.selectedUnit.substring(0, 1)}',
                    '${(alarm.max ?? sensor.rangeMax).toStringAsFixed(1)} ${sensor.selectedUnit.substring(0, 1)}'),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  children: [
                    DropdownButton(
                      value: alarm.type,
                      onChanged: entry.key == 0
                          ? (value) {
                              setType(value, sensor);
                            }
                          : (_) {},
                      items: entry.key == 0
                          ? AlarmType.values.map((e) {
                              return DropdownMenuItem<AlarmType>(
                                value: e,
                                child: Text(e.toString().split('.').last),
                              );
                            }).toList()
                          : [
                              DropdownMenuItem<AlarmType>(
                                value: alarm.type,
                                child:
                                    Text(alarm.type.toString().split('.').last),
                              ),
                            ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 16),
                    ),
                    SizedBox(
                      width: 48,
                      child: TextField(
                        controller: alarm.minController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 8,
                            vertical: 8,
                          ),
                          border: OutlineInputBorder(),
                          isDense: true,
                        ),
                        keyboardType: TextInputType.number,
                        onChanged: (value) => setState(() {
                          final val = double.tryParse(value);
                          if (val == null) return;
                          alarm.min = val;
                          sensor.clamp();
                        }),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 16),
                    ),
                    SizedBox(
                      width: 56,
                      child: TextField(
                        controller: alarm.maxController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 8,
                            vertical: 8,
                          ),
                          border: OutlineInputBorder(),
                          isDense: true,
                        ),
                        keyboardType: TextInputType.number,
                        onChanged: (value) => setState(
                          () {
                            final val = double.tryParse(value);
                            if (val == null) return;
                            alarm.max = val;
                            sensor.clamp();
                            print('Sensor: ${sensor}');
                          },
                        ),
                        inputFormatters: [
                          FilteringTextInputFormatter(RegExp('[0-9]|.'),
                              allow: true)
                        ],
                      ),
                    ),
                    Expanded(child: Container()),
                    IconButton(
                      icon: Icon(Icons.delete),
                      iconSize: 36,
                      padding: EdgeInsets.only(
                        bottom: _height * 0.008,
                        right: _width * 0.04,
                      ),
                      color: Colors.red,
                      onPressed: () {
                        sensor.alarms.remove(alarm);
                        setState(() {});
                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.cleaning_services_outlined),
                      iconSize: 32,
                      padding: EdgeInsets.only(
                        bottom: _height * 0.012,
                        right: _width * 0.02,
                      ),
                      color: Colors.blue[900],
                      onPressed: () {
                        alarm.min = sensor.rangeMin;
                        alarm.max = sensor.rangeMax;
                        sensor.clamp();
                        setState(() {});
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  addAlarm(Sensor sensor) async {
    if (sensor.alarms.length == 0)
      sensor.alarms.add(Alarm(
        type: AlarmType.Critical,
        value: sensor.rangeMax,
        min: sensor.rangeMin,
        max: sensor.rangeMax,
      ));
    else if (sensor.alarms.length == 1) {
      final isCritical =
          !sensor.alarms.any((e) => e.type == AlarmType.Critical);
      if (isCritical)
        sensor.alarms.add(Alarm(
          type: AlarmType.Critical,
          value: sensor.rangeMin,
          min: sensor.rangeMin,
          max: sensor.rangeMax,
        ));
      else
        sensor.alarms.add(Alarm(
            type: AlarmType.Non,
            value: sensor.rangeMax,
            min: sensor.rangeMin,
            max: sensor.rangeMax));
    }
    sensor.clamp();
    setState(() {});
  }

  setType(AlarmType type, Sensor sensor) {
    final alt = type == AlarmType.Non ? AlarmType.Critical : AlarmType.Non;
    sensor.alarms.first.type = type;
    if (sensor.alarms.length == 2) {
      sensor.alarms[1].type = alt;
    }
    sensor.clamp();
    setState(() {});
  }

  snackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(text),
        duration: Duration(seconds: 2),
      ),
    );
  }
}
