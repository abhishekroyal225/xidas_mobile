import 'dart:async';
import 'dart:convert';
import 'package:ZettaWifiDirect/model/saved_sensor_data.dart';

import '../services/Services.dart';
import 'package:http/http.dart' as http;

import '../sensor_screens/sensor_setup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../model/devices_model.dart';

class DevicesList extends StatefulWidget {
  final String title;
  DevicesList({this.title});

  @override
  _DevicesListDashboardState createState() => _DevicesListDashboardState();
}

class Debouncer {
  final int milliSeconds;
  VoidCallback callback;
  Timer _timer;

  Debouncer({this.milliSeconds});

  run(VoidCallback callback) {
    if (null != _timer) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliSeconds), callback);
  }
}

class _DevicesListDashboardState extends State<DevicesList> {
  double _height, _width;
  final _debouncer = Debouncer(milliSeconds: 500);
  bool _isScanningDevices = true;
  List<DevicesModel> _devicesList;
  List<String> _customersList = [
    'XID_DEVICE_001',
    'XID_DEVICE_002',
    'XID_DEVICE_003',
    'XID_DEVICE_004',
    'XID_DEVICE_005',
    'XID_DEVICE_006',
    'XID_DEVICE_007',
    'XID_DEVICE_008',
    'XID_DEVICE_009',
    'XID_DEVICE_010',
    'XID_DEVICE_011',
    'XID_DEVICE_012',
    'XID_DEVICE_013',
    'XID_DEVICE_014',
    'XID_DEVICE_015',
    'XID_DEVICE_016',
    'XID_DEVICE_017',
    'XID_DEVICE_018'
  ];

  List<DevicesModel> _filteredDevices;
  List<String> _filteredDevices1;

  @override
  void initState() {
    super.initState();
    Services.getDevices().then((devicesFromServer) {
      setState(() {
        _devicesList = devicesFromServer;
        _filteredDevices = _devicesList;
        _isScanningDevices = false;
      });
    });
    // _isScanningDevices = false;
    // _filteredDevices1 = _customersList;
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    // _filteredDevices1 = _customersList;
    return Scaffold( // constarint layout
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
        actions: <Widget>[
          Row(
            children: <Widget>[
              IconButton(
                icon: Stack(
                  children: [
                    Icon(
                      Icons.notifications,
                      color: Colors.white,
                    ),
                  ],
                ),
                onPressed: () => print('Pressed'),
              )
            ],
          )
        ],
      ),
      body: Container(
        margin: EdgeInsets.only(
          top: _height * 0.02,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Center(
                child: Text(
                  'Xidas Devices List',
                  style: TextStyle(
                    color: Colors.blue,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: _height * 0.01,
            ),
            Container(
              padding: EdgeInsets.all(14.0),
              child: TextField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  labelText: 'Search Device',
                  hintText: 'Enter Device Name',
                ),
                onChanged: (string) {
                  _debouncer.run(() {
                    setState(() {
                      print(_customersList);
                      _filteredDevices = _devicesList
                          .where((element) => (element.deviceName
                              .toString()
                              .toLowerCase()
                              .contains(string.toLowerCase())))
                          .toList();
                      //
                      // _filteredDevices1 = _customersList
                      //     .where((element) => element
                      //         .toString()
                      //         .toLowerCase()
                      //         .contains(string.toLowerCase()))
                      //     .toList();
                    });
                  });
                },
              ),
            ),
            _isScanningDevices
                ? Expanded(
                    child: Center(
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.black,
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(Colors.blue),
                      ),
                    ),
                  )
                : Expanded(
                    child: ListView.builder(
                      padding: EdgeInsets.all(10.0),
                      itemCount: _filteredDevices.length,
                      // itemCount: _filteredDevices1.length,
                      itemBuilder: ((BuildContext context, int index) {
                        print('_filteredDevices.length>>>' +
                            _filteredDevices.length.toString());
                        return Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: Colors.blue,
                          elevation: 3,
                          child: InkWell(
                            child: Container(
                              height: _height * 0.06,
                              width: _width,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    padding:
                                        EdgeInsets.only(left: _width * 0.04),
                                    child: Text(
                                      _filteredDevices[index].deviceName,
                                      // _filteredDevices1[index],
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(right: _width * 0.02),
                                    child: Icon(
                                      Icons.arrow_forward,
                                      size: 30,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () async {
                              // Request API (GET /get-device-config-data-by-id)
                              int deviceId = _filteredDevices[index].deviceId;

                              var savedDataByDeviceIdData = await http.get(
                                  Uri.parse(
                                      'https://34.213.13.191:8443/get-device-config-data-by-id?deviceid=$deviceId'),
                                  headers: {"Accept": "application/json"});

                             final sensorDataJson =
                                  jsonDecode(savedDataByDeviceIdData.body);

                              if (sensorDataJson.length > 0) {
                                // TODO: Set Data Here
                                SavedSensorData sensorData =
                                    SavedSensorData.fromJson(sensorDataJson[0]);

                                Navigator.pushAndRemoveUntil(
                                    context,
                                    CupertinoPageRoute(
                                      builder: (context) => SensorSetup(
                                        title: 'XIDAS IOT Mobile App',
                                        deviceId:
                                            _filteredDevices[index].deviceId,
                                        isDataGotFromSavedData: true,
                                        sensorData: sensorData,
                                      ),
                                    ),
                                    (route) => true);
                              } else {
                                var viewDeviceByIdData = await http.get(
                                    Uri.parse(
                                        'https://34.213.13.191:8443/view-device-echip-by-id?deviceid=$deviceId'),
                                    headers: {"Accept": "application/json"});

                                if (viewDeviceByIdData.statusCode == 200) {
                                  Map<String, dynamic> viewSensorDataJson =
                                      jsonDecode(viewDeviceByIdData.body)[0];
                                  SavedSensorData viewSensorData =
                                      SavedSensorData.fromJson(
                                          viewSensorDataJson);
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      CupertinoPageRoute(
                                        builder: (context) => SensorSetup(
                                          title: 'XIDAS IOT Mobile App',
                                          deviceId:
                                              _filteredDevices[index].deviceId,
                                          isDataGotFromSavedData: false,
                                          sensorData: viewSensorData,
                                        ),
                                      ),
                                      (route) => true);
                                } else {
                                  print('Server Error!');
                                }
                              }
                            },
                          ),
                        );
                      }),
                    ),
                  )
          ],
        ),
      ),
    );
  }
}
