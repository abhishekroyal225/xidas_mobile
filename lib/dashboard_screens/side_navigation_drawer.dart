import 'package:ZettaWifiDirect/main_screens/Login.dart';
import 'package:ZettaWifiDirect/main_screens/WiFiDevices.dart';
import 'package:ZettaWifiDirect/login_screens/change_password.dart';
import 'package:ZettaWifiDirect/util/progressDialogBox.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'dart:io' as io;

class SideNavigationDrawer extends StatefulWidget {
  final BuildContext context;
  SideNavigationDrawer(this.context);
  @override
  _SideNavigationDrawerState createState() => _SideNavigationDrawerState();
}

class _SideNavigationDrawerState extends State<SideNavigationDrawer> {
  List<io.FileSystemEntity> files;
  String firmwareFile;

  List<String> _menuList = [
    'Device Provisioning',
    'Upload Firmware',
    'Change Password',
    'Help',
    'About Us',
    'Logout'
  ];
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(20),
            color: Theme.of(context).primaryColor,
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(
                      top: 30,
                    ),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: Image(
                      image: AssetImage('assets/images/profile_pic_1.png'),
                      fit: BoxFit.fill,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
              child: ListView.builder(
                  padding: EdgeInsets.all(10.0),
                  itemCount: _menuList.length,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, int index) {
                    return Card(
                      shape: RoundedRectangleBorder(
                        // side: new BorderSide(
                        //     color: Colors.blue, width: 2.0),
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      elevation: 3,
                      child: Padding(
                        padding: EdgeInsets.all(4.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ListTile(
                              title: Text(
                                _menuList[index],
                                style: TextStyle(
                                  fontSize: 18,
                                ),
                              ),
                              trailing: Icon(Icons.keyboard_arrow_right),
                              onTap: () async {
                                print('_menu List Index>>${_menuList[index]}');
                                Navigator.of(context).pop();
                                if (_menuList[index] == 'Device Provisioning') {
                                  ProgressDialogBox.showPopUpDialog(
                                      widget.context,
                                      'Status',
                                      'Under Development');
                                } else if (_menuList[index] ==
                                    'Upload Firmware') {
                                  final extDir =
                                      await getExternalStorageDirectory();
                                  final filePath =
                                      '${extDir.path}/xidas_firmwares/';
                                  files = io.Directory(filePath).listSync();
                                  for (var file in files) {
                                    if (file.path.contains('.bin'))
                                      firmwareFile = file.path;
                                  }

                                  if (firmwareFile != null) {
                                    ProgressDialogBox
                                        .showCircularProgressIndicator(
                                            widget.context,
                                            'Uploading the firmware');
                                    int responseStatusCode =
                                        await uploadFirmwareToHost(
                                            firmwareFile);

                                    if (responseStatusCode == 200) {
                                      Navigator.of(widget.context).pop();
                                      ProgressDialogBox.showPopUpDialog(
                                          widget.context,
                                          'Firmware Status',
                                          'Firmware Uploaded Successfully');
                                    } else if (responseStatusCode == 400) {
                                      Navigator.of(widget.context).pop();
                                      ProgressDialogBox
                                          .showUploadFirmwareAgainDialog(
                                              widget.context,
                                              'Firmware already exists',
                                              'Do you want to upload again?',
                                              () {
                                        Navigator.pop(widget.context);
                                      }, () async {
                                        Navigator.pop(widget.context);
                                        ProgressDialogBox
                                            .showCircularProgressIndicator(
                                                widget.context,
                                                'Uploading the firmware');
                                        int responseCode =
                                            await deleteFirmwareFromHost(
                                                firmwareFile);
                                        if (responseCode == 200) {
                                          int responseStatusCode =
                                              await uploadFirmwareToHost(
                                                  firmwareFile);

                                          if (responseStatusCode == 200) {
                                            Navigator.of(widget.context).pop();
                                            ProgressDialogBox.showPopUpDialog(
                                                widget.context,
                                                'Firmware Status',
                                                'Firmware Uploaded Successfully');
                                          } else {
                                            Navigator.of(widget.context).pop();
                                            ProgressDialogBox.showPopUpDialog(
                                                widget.context,
                                                'Firmware Status',
                                                'Upload Failed');
                                          }
                                        }
                                      });
                                    } else {
                                      Navigator.of(widget.context).pop();
                                      ProgressDialogBox.showPopUpDialog(
                                          widget.context,
                                          'Firmware Status',
                                          'Upload Failed');
                                    }
                                  }
                                } else if (_menuList[index] ==
                                    'Change Password')
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      CupertinoPageRoute(
                                        builder: (context) => ChangePassword(
                                          title: 'XIDAS IOT Mobile App',
                                        ),
                                      ),
                                      (route) => true);
                                else if (_menuList[index] == 'Help') {
                                  Container();
                                } else {
                                  if (_menuList[index] == 'About Us')
                                  ProgressDialogBox.showPopUpDialog(widget.context, 'Warning', 'Under Development');
                                  else
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        CupertinoPageRoute(
                                          builder: (context) => Login(
                                            title: 'XIDAS IOT Mobile App',
                                          ),
                                        ),
                                        (route) => true);
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    );
                  })),
        ],
      ),
    );
  }

  Future<int> uploadFirmwareToHost(String filePath) async {
    try {
      var fileName = getFileName(filePath);
      var result = await http.post(
        Uri.parse('http://192.168.4.1:8888/upload/$fileName'),
        body: io.File(filePath).readAsBytesSync(),
      );
      return result.statusCode;
    } catch (e) {
      return 400; //handle exception
    }
  }

  Future<int> deleteFirmwareFromHost(String filePath) async {
    String fileName = getFileName(filePath);
    var result = await http
        .delete(Uri.parse('http://192.168.4.1:8888/Delete?filename=$fileName'));
    return result.statusCode;
  }

  String getFileName(String filePath) {
    return filePath.split('/').last;
  }
}
