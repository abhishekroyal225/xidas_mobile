// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'measurement_rate_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MeasurementRate _$MeasurementRateFromJson(Map<String, dynamic> json) {
  return MeasurementRate(
    measurementRateId: json['measurementRateId'] as int,
    measurementRate: json['measurementRate'] as int,
  );
}

Map<String, dynamic> _$MeasurementRateToJson(MeasurementRate instance) =>
    <String, dynamic>{
      'measurementRateId': instance.measurementRateId,
      'measurementRate': instance.measurementRate,
    };
