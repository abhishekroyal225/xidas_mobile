import 'package:ZettaWifiDirect/model/measurement_rate_model.dart';
import 'package:ZettaWifiDirect/model/transmit_rate_model.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'sensor_model.g.dart';

@JsonSerializable()
class Sensor {
  @JsonKey(name: 'sensorTypeId')
  int typeId;
  @JsonKey(name: 'sensorName')
  String name;
  @JsonKey(name: 'unit', toJson: toNull, includeIfNull: false)
  List<String> units;
  List<Alarm> alarms = [];
  @JsonKey(ignore: true)
  double rangeMin;
  @JsonKey(ignore: true)
  double rangeMax;
  MeasurementRate measurementRate;
  TransmitRate transmitRate;
  String selectedUnit;
  Sensor({
    this.typeId,
    this.name,
    this.units,
    this.rangeMin,
    this.rangeMax,
    this.alarms,
    this.selectedUnit,
  }) {
    this.rangeMin = rangeMin ?? 0;
    this.rangeMax = rangeMax ?? 150;
    this.units = units ?? ['Unit'];
    this.selectedUnit = units.first;
    this.alarms = alarms ?? [];
  }

  factory Sensor.fromJson(Map<String, dynamic> json) => _$SensorFromJson(json);

  Map<String, dynamic> toJson() => _$SensorToJson(this);
  
  clamp() {
    this.alarms.forEach((alarm) {
      if (alarm.type == AlarmType.Critical) {
        // return;
        alarm.min = alarm.min.clamp(rangeMin, alarm.max);
        alarm.max = alarm.max.clamp(alarm.min, this.rangeMax);
      } else if (alarms.length > 1) {
        alarm.min = alarm.min.clamp(
          (this.alarms.firstWhere((e) => e.type == AlarmType.Critical)?.min ??
                  rangeMin) +
              1,
          alarm.max,
        );
        alarm.max = alarm.max.clamp(
          alarm.min,
          (this.alarms.firstWhere((e) => e.type == AlarmType.Critical)?.max ??
                  rangeMax) -
              1,
        );
      }
      alarm.minController.text = alarm.min.toStringAsFixed(1);
      alarm.maxController.text = alarm.max.toStringAsFixed(1);
    });

    print('Clamped $this');
  }
}

class SensorInfo {
  String name;
  List<Alarm> alarms = [];
  double min;
  double max;
  double criticalLower;
  double criticalUpper;
  String unit;
  double rangeMin;
  double rangeMax;

  SensorInfo(
    this.name,
    this.min,
    this.max,
    this.unit, {
    this.criticalLower,
    this.criticalUpper,
    this.rangeMin,
    this.rangeMax,
  }) {
    this.rangeMax = rangeMax ?? max ?? 150;
    this.rangeMin = rangeMin ?? min ?? 0;
  }

  clamp() {
    this.alarms.forEach((alarm) {
      if (alarm.type == AlarmType.Critical) {
        // return;
        alarm.min = alarm.min.clamp(rangeMin, alarm.max);
        alarm.max = alarm.max.clamp(alarm.min, this.rangeMax);
      } else {
        alarm.min = alarm.min.clamp(
          (this.alarms.firstWhere((e) => e.type == AlarmType.Critical)?.min ??
                  rangeMin) +
              1,
          alarm.max,
        );
        alarm.max = alarm.max.clamp(
          alarm.min,
          (this.alarms.firstWhere((e) => e.type == AlarmType.Critical)?.max ??
                  rangeMax) -
              1,
        );
      }
      alarm.minController.text = alarm.min.toStringAsFixed(1);
      alarm.maxController.text = alarm.max.toStringAsFixed(1);
    });

    print('Clamped $this');
  }

  @override
  String toString() {
    var str = name;
    for (var value in alarms) {
      str += '\n' + value.toString();
    }
    return str;
  }
}

@JsonSerializable()
class Alarm {
  AlarmType type;
  double value;
  double min;
  double max;

  @JsonKey(ignore: true)
  TextEditingController minController = TextEditingController();
  @JsonKey(ignore: true)
  TextEditingController maxController = TextEditingController();

  Alarm({this.type, this.value, this.min, this.max}) {
    this.minController.text = this.min?.toString();
    this.maxController.text = this.max?.toString();
  }

  factory Alarm.fromJson(Map<String, dynamic> json) => _$AlarmFromJson(json);

  Map<String, dynamic> toJson() => _$AlarmToJson(this);

  @override
  String toString() {
    return '$type Min: $min, Max: $max';
  }
}

enum AlarmType { Critical, Non }

toNull(_) => null;
