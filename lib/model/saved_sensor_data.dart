import 'package:json_annotation/json_annotation.dart';

part 'saved_sensor_data.g.dart';

@JsonSerializable()
class SavedSensorData {
  SavedSensorData({
    this.deviceConfigId,
    this.deviceId,
    this.deviceName,
    this.deviceType,
    this.macId,
    this.hostId,
    this.partNumber,
    this.hostType,
    this.hostName,
    this.serialNumber,
    this.firmwareVersion,
    this.firmwareType,
    this.creationTime,
    this.deleteFlag,
    this.echipDetails,
  });

  int deviceConfigId;
  int deviceId;
  String deviceName;
  String deviceType;
  String macId;
  int hostId;
  String partNumber;
  String hostType;
  String hostName;
  String serialNumber;
  String firmwareVersion;
  String firmwareType;
  int creationTime;
  int deleteFlag;
  List<EchipDetail> echipDetails;

  factory SavedSensorData.fromJson(Map<String, dynamic> json) =>
      _$SavedSensorDataFromJson(json);

  Map<String, dynamic> toJson() => _$SavedSensorDataToJson(this);
}

@JsonSerializable()
class EchipDetail {
  EchipDetail({
    this.deviceConfigEchipId,
    this.echipId,
    this.echipName,
    this.echipVersion,
    this.echipSerialNumber,
    this.sensorDetails,
  });

  int deviceConfigEchipId;
  int echipId;
  String echipName;
  String echipVersion;
  String echipSerialNumber;
  List<SensorDetail> sensorDetails;

  factory EchipDetail.fromJson(Map<String, dynamic> json) =>
      _$EchipDetailFromJson(json);

  Map<String, dynamic> toJson() => _$EchipDetailToJson(this);
}

@JsonSerializable()
class SensorDetail {
  SensorDetail({
    this.deviceConfigEchipSensorId,
    this.sensorTypeId,
    this.sensorName,
    this.alarms,
    this.measurementRate,
    this.transmitRate,
    this.selectedUnit,
  });

  int deviceConfigEchipSensorId;
  int sensorTypeId;
  dynamic sensorName;
  List<SavedAlarm> alarms;
  SavedMeasurementRate measurementRate;
  SavedTransmitRate transmitRate;
  String selectedUnit;

  factory SensorDetail.fromJson(Map<String, dynamic> json) =>
      _$SensorDetailFromJson(json);

  Map<String, dynamic> toJson() => _$SensorDetailToJson(this);
}

@JsonSerializable()
class SavedAlarm {
  SavedAlarm({
    this.deviceConfigEchipSensorAlarmId,
    this.type,
    this.value,
    this.min,
    this.max,
  });

  int deviceConfigEchipSensorAlarmId;
  String type;
  double value;
  double min;
  double max;

  factory SavedAlarm.fromJson(Map<String, dynamic> json) =>
      _$SavedAlarmFromJson(json);

  Map<String, dynamic> toJson() => _$SavedAlarmToJson(this);
}

@JsonSerializable()
class SavedMeasurementRate {
  SavedMeasurementRate({
    this.deviceConfigEchipSensorMeasurementId,
    this.measurementRateId,
    this.measurementRate,
  });

  int deviceConfigEchipSensorMeasurementId;
  int measurementRateId;
  int measurementRate;

  factory SavedMeasurementRate.fromJson(Map<String, dynamic> json) =>
      _$SavedMeasurementRateFromJson(json);

  Map<String, dynamic> toJson() => _$SavedMeasurementRateToJson(this);
}

@JsonSerializable()
class SavedTransmitRate {
  SavedTransmitRate({
    this.deviceConfigEchipSensorTransmitId,
    this.transmitRateId,
    this.transmitRate,
  });

  int deviceConfigEchipSensorTransmitId;
  int transmitRateId;
  int transmitRate;

  factory SavedTransmitRate.fromJson(Map<String, dynamic> json) =>
      _$SavedTransmitRateFromJson(json);

  Map<String, dynamic> toJson() => _$SavedTransmitRateToJson(this);
}
