// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) {
  return LoginResponse(
    json['userId'] as int,
    json['userRoleId'] as String,
    json['userType'] as String,
    json['firstName'] as String,
    json['lastName'] as String,
    json['customerId'] as int,
    json['position'] as String,
    json['email'] as String,
    json['message'] as String,
    json['responseCode'] as int,
    json['isError'] as bool,
  );
}

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'userRoleId': instance.userRoleId,
      'userType': instance.userType,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'customerId': instance.customerId,
      'position': instance.position,
      'email': instance.email,
      'message': instance.message,
      'responseCode': instance.responseCode,
      'isError': instance.isError,
    };
