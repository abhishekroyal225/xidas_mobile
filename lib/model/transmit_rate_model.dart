import 'package:json_annotation/json_annotation.dart';

part 'transmit_rate_model.g.dart';

@JsonSerializable()
class TransmitRate {
  int transmitRateId;
  int transmitRate;
  @JsonKey(ignore: true)
  int creationTime;
  @JsonKey(ignore: true)
  int deleteFlag;

  TransmitRate({
    this.transmitRateId,
    this.transmitRate,
    this.creationTime,
    this.deleteFlag,
  });

  factory TransmitRate.fromJson(Map<String, dynamic> json) =>
      _$TransmitRateFromJson(json);

  Map<String, dynamic> toJson() => _$TransmitRateToJson(this);
}
