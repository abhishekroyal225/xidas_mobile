// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transmit_rate_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransmitRate _$TransmitRateFromJson(Map<String, dynamic> json) {
  return TransmitRate(
    transmitRateId: json['transmitRateId'] as int,
    transmitRate: json['transmitRate'] as int,
  );
}

Map<String, dynamic> _$TransmitRateToJson(TransmitRate instance) =>
    <String, dynamic>{
      'transmitRateId': instance.transmitRateId,
      'transmitRate': instance.transmitRate,
    };
