// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sensor_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Sensor _$SensorFromJson(Map<String, dynamic> json) {
  return Sensor(
    typeId: json['sensorTypeId'] as int,
    name: json['sensorName'] as String,
    units: (json['unit'] as List)?.map((e) => e as String)?.toList(),
    alarms: (json['alarms'] as List)
        ?.map(
            (e) => e == null ? null : Alarm.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    selectedUnit: json['selectedUnit'] as String,
  )
    ..measurementRate = json['measurementRate'] == null
        ? null
        : MeasurementRate.fromJson(
            json['measurementRate'] as Map<String, dynamic>)
    ..transmitRate = json['transmitRate'] == null
        ? null
        : TransmitRate.fromJson(json['transmitRate'] as Map<String, dynamic>);
}

Map<String, dynamic> _$SensorToJson(Sensor instance) {
  final val = <String, dynamic>{
    'sensorTypeId': instance.typeId,
    'sensorName': instance.name,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('unit', toNull(instance.units));
  val['alarms'] = instance.alarms;
  val['measurementRate'] = instance.measurementRate;
  val['transmitRate'] = instance.transmitRate;
  val['selectedUnit'] = instance.selectedUnit;
  return val;
}

Alarm _$AlarmFromJson(Map<String, dynamic> json) {
  return Alarm(
    type: _$enumDecodeNullable(_$AlarmTypeEnumMap, json['type']),
    value: (json['value'] as num)?.toDouble(),
    min: (json['min'] as num)?.toDouble(),
    max: (json['max'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$AlarmToJson(Alarm instance) => <String, dynamic>{
      'type': _$AlarmTypeEnumMap[instance.type],
      'value': instance.value,
      'min': instance.min,
      'max': instance.max,
    };

T _$enumDecode<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }

  final value = enumValues.entries
      .singleWhere((e) => e.value == source, orElse: () => null)
      ?.key;

  if (value == null && unknownValue == null) {
    throw ArgumentError('`$source` is not one of the supported values: '
        '${enumValues.values.join(', ')}');
  }
  return value ?? unknownValue;
}

T _$enumDecodeNullable<T>(
  Map<T, dynamic> enumValues,
  dynamic source, {
  T unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source, unknownValue: unknownValue);
}

const _$AlarmTypeEnumMap = {
  AlarmType.Critical: 'Critical',
  AlarmType.Non: 'Non',
};
