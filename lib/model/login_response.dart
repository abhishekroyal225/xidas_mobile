import 'package:json_annotation/json_annotation.dart';
part 'login_response.g.dart';

@JsonSerializable()
class LoginResponse {
  @JsonKey(name: 'userId')
  final int userId;

  @JsonKey(name: 'userRoleId')
  final String userRoleId;

  @JsonKey(name: 'userType')
  final String userType;

  @JsonKey(name: 'firstName')
  final String firstName;

  @JsonKey(name: 'lastName')
  final String lastName;

  @JsonKey(name: 'customerId')
  final int customerId;

  @JsonKey(name: 'position')
  final String position;

  @JsonKey(name: 'email')
  final String email;

  @JsonKey(name: 'message')
  final String message;

  @JsonKey(name: 'responseCode')
  final int responseCode;

  @JsonKey(name: 'isError')
  final bool isError;

  LoginResponse(
      this.userId,
      this.userRoleId,
      this.userType,
      this.firstName,
      this.lastName,
      this.customerId,
      this.position,
      this.email,
      this.message,
      this.responseCode,
      this.isError);

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);
}
