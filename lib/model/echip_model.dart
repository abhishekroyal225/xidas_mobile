import 'package:ZettaWifiDirect/model/sensor_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'echip_model.g.dart';

@JsonSerializable()
class EChip {
  @JsonKey(name: 'echipId')
  int id;
  @JsonKey(name: 'echipName')
  String name;
  @JsonKey(name: 'echipVersion')
  String version;
  @JsonKey(name: 'echipSerialNumber')
  String serialNumber;

  List<Sensor> sensorDetails;

  EChip(
      {this.id,
      this.name,
      this.version,
      this.serialNumber,
      this.sensorDetails});

  factory EChip.fromJson(Map<String, dynamic> json) => _$EChipFromJson(json);

  Map<String, dynamic> toJson() => _$EChipToJson(this);
}
