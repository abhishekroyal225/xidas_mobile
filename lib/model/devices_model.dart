import 'package:ZettaWifiDirect/model/echip_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'devices_model.g.dart';

@JsonSerializable()
class DevicesModel {
  int deviceId;
  String macId;
  String deviceType;
  String deviceName;
  int hostId;
  String partNumber;
  String hostType;
  String hostName;
  String serialNumber;
  String firmwareVersion;
  String firmwareType;
  int creationTime;
  int deleteFlag;

  List<EChip> eChipDetails;

  DevicesModel({
    this.deviceId,
    this.macId,
    this.deviceType,
    this.deviceName,
    this.hostId,
    this.partNumber,
    this.hostType,
    this.hostName,
    this.serialNumber,
    this.firmwareVersion,
    this.firmwareType,
    this.creationTime,
    this.deleteFlag,
    this.eChipDetails,
  });

  factory DevicesModel.fromJson(Map<String, dynamic> json) =>
      _$DevicesModelFromJson(json);

  Map<String, dynamic> toJson() => _$DevicesModelToJson(this);
}
