// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'echip_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EChip _$EChipFromJson(Map<String, dynamic> json) {
  return EChip(
    id: json['echipId'] as int,
    name: json['echipName'] as String,
    version: json['echipVersion'] as String,
    serialNumber: json['echipSerialNumber'] as String,
    sensorDetails: (json['sensorDetails'] as List)
        ?.map((e) =>
            e == null ? null : Sensor.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$EChipToJson(EChip instance) => <String, dynamic>{
      'echipId': instance.id,
      'echipName': instance.name,
      'echipVersion': instance.version,
      'echipSerialNumber': instance.serialNumber,
      'sensorDetails': instance.sensorDetails,
    };
