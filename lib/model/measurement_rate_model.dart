import 'package:json_annotation/json_annotation.dart';

part 'measurement_rate_model.g.dart';

@JsonSerializable()
class MeasurementRate {
  int measurementRateId;
  int measurementRate;
  @JsonKey(ignore: true)
  int creationTime;
  @JsonKey(ignore: true)
  int deleteFlag;

  MeasurementRate(
      {this.measurementRateId,
      this.measurementRate,
      this.creationTime,
      this.deleteFlag});

  factory MeasurementRate.fromJson(Map<String, dynamic> json) =>
      _$MeasurementRateFromJson(json);

  Map<String, dynamic> toJson() => _$MeasurementRateToJson(this);
}
