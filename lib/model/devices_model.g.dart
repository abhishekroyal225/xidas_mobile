// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'devices_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DevicesModel _$DevicesModelFromJson(Map<String, dynamic> json) {
  return DevicesModel(
    deviceId: json['deviceId'] as int,
    macId: json['macId'] as String,
    deviceType: json['deviceType'] as String,
    deviceName: json['deviceName'] as String,
    hostId: json['hostId'] as int,
    partNumber: json['partNumber'] as String,
    hostType: json['hostType'] as String,
    hostName: json['hostName'] as String,
    serialNumber: json['serialNumber'] as String,
    firmwareVersion: json['firmwareVersion'] as String,
    firmwareType: json['firmwareType'] as String,
    creationTime: json['creationTime'] as int,
    deleteFlag: json['deleteFlag'] as int,
    eChipDetails: (json['eChipDetails'] as List)
        ?.map(
            (e) => e == null ? null : EChip.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$DevicesModelToJson(DevicesModel instance) =>
    <String, dynamic>{
      'deviceId': instance.deviceId,
      'macId': instance.macId,
      'deviceType': instance.deviceType,
      'deviceName': instance.deviceName,
      'hostId': instance.hostId,
      'partNumber': instance.partNumber,
      'hostType': instance.hostType,
      'hostName': instance.hostName,
      'serialNumber': instance.serialNumber,
      'firmwareVersion': instance.firmwareVersion,
      'firmwareType': instance.firmwareType,
      'creationTime': instance.creationTime,
      'deleteFlag': instance.deleteFlag,
      'eChipDetails': instance.eChipDetails,
    };
