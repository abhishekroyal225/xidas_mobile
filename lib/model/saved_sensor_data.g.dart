// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'saved_sensor_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SavedSensorData _$SavedSensorDataFromJson(Map<String, dynamic> json) {
  return SavedSensorData(
    deviceConfigId: json['deviceConfigId'] as int,
    deviceId: json['deviceId'] as int,
    deviceName: json['deviceName'] as String,
    deviceType: json['deviceType'] as String,
    macId: json['macId'] as String,
    hostId: json['hostId'] as int,
    partNumber: json['partNumber'] as String,
    hostType: json['hostType'] as String,
    hostName: json['hostName'] as String,
    serialNumber: json['serialNumber'] as String,
    firmwareVersion: json['firmwareVersion'] as String,
    firmwareType: json['firmwareType'] as String,
    creationTime: json['creationTime'] as int,
    deleteFlag: json['deleteFlag'] as int,
    echipDetails: (json['echipDetails'] as List)
        ?.map((e) =>
            e == null ? null : EchipDetail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$SavedSensorDataToJson(SavedSensorData instance) =>
    <String, dynamic>{
      'deviceConfigId': instance.deviceConfigId,
      'deviceId': instance.deviceId,
      'deviceName': instance.deviceName,
      'deviceType': instance.deviceType,
      'macId': instance.macId,
      'hostId': instance.hostId,
      'partNumber': instance.partNumber,
      'hostType': instance.hostType,
      'hostName': instance.hostName,
      'serialNumber': instance.serialNumber,
      'firmwareVersion': instance.firmwareVersion,
      'firmwareType': instance.firmwareType,
      'creationTime': instance.creationTime,
      'deleteFlag': instance.deleteFlag,
      'echipDetails': instance.echipDetails,
    };

EchipDetail _$EchipDetailFromJson(Map<String, dynamic> json) {
  return EchipDetail(
    deviceConfigEchipId: json['deviceConfigEchipId'] as int,
    echipId: json['echipId'] as int,
    echipName: json['echipName'] as String,
    echipVersion: json['echipVersion'] as String,
    echipSerialNumber: json['echipSerialNumber'] as String,
    sensorDetails: (json['sensorDetails'] as List)
        ?.map((e) =>
            e == null ? null : SensorDetail.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$EchipDetailToJson(EchipDetail instance) =>
    <String, dynamic>{
      'deviceConfigEchipId': instance.deviceConfigEchipId,
      'echipId': instance.echipId,
      'echipName': instance.echipName,
      'echipVersion': instance.echipVersion,
      'echipSerialNumber': instance.echipSerialNumber,
      'sensorDetails': instance.sensorDetails,
    };

SensorDetail _$SensorDetailFromJson(Map<String, dynamic> json) {
  return SensorDetail(
    deviceConfigEchipSensorId: json['deviceConfigEchipSensorId'] as int,
    sensorTypeId: json['sensorTypeId'] as int,
    sensorName: json['sensorName'],
    alarms: (json['alarms'] as List)
        ?.map((e) =>
            e == null ? null : SavedAlarm.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    measurementRate: json['measurementRate'] == null
        ? null
        : SavedMeasurementRate.fromJson(
            json['measurementRate'] as Map<String, dynamic>),
    transmitRate: json['transmitRate'] == null
        ? null
        : SavedTransmitRate.fromJson(
            json['transmitRate'] as Map<String, dynamic>),
    selectedUnit: json['selectedUnit'] as String,
  );
}

Map<String, dynamic> _$SensorDetailToJson(SensorDetail instance) =>
    <String, dynamic>{
      'deviceConfigEchipSensorId': instance.deviceConfigEchipSensorId,
      'sensorTypeId': instance.sensorTypeId,
      'sensorName': instance.sensorName,
      'alarms': instance.alarms,
      'measurementRate': instance.measurementRate,
      'transmitRate': instance.transmitRate,
      'selectedUnit': instance.selectedUnit,
    };

SavedAlarm _$SavedAlarmFromJson(Map<String, dynamic> json) {
  return SavedAlarm(
    deviceConfigEchipSensorAlarmId:
        json['deviceConfigEchipSensorAlarmId'] as int,
    type: json['type'] as String,
    value: (json['value'] as num)?.toDouble(),
    min: (json['min'] as num)?.toDouble(),
    max: (json['max'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$SavedAlarmToJson(SavedAlarm instance) =>
    <String, dynamic>{
      'deviceConfigEchipSensorAlarmId': instance.deviceConfigEchipSensorAlarmId,
      'type': instance.type,
      'value': instance.value,
      'min': instance.min,
      'max': instance.max,
    };

SavedMeasurementRate _$SavedMeasurementRateFromJson(Map<String, dynamic> json) {
  return SavedMeasurementRate(
    deviceConfigEchipSensorMeasurementId:
        json['deviceConfigEchipSensorMeasurementId'] as int,
    measurementRateId: json['measurementRateId'] as int,
    measurementRate: json['measurementRate'] as int,
  );
}

Map<String, dynamic> _$SavedMeasurementRateToJson(
        SavedMeasurementRate instance) =>
    <String, dynamic>{
      'deviceConfigEchipSensorMeasurementId':
          instance.deviceConfigEchipSensorMeasurementId,
      'measurementRateId': instance.measurementRateId,
      'measurementRate': instance.measurementRate,
    };

SavedTransmitRate _$SavedTransmitRateFromJson(Map<String, dynamic> json) {
  return SavedTransmitRate(
    deviceConfigEchipSensorTransmitId:
        json['deviceConfigEchipSensorTransmitId'] as int,
    transmitRateId: json['transmitRateId'] as int,
    transmitRate: json['transmitRate'] as int,
  );
}

Map<String, dynamic> _$SavedTransmitRateToJson(SavedTransmitRate instance) =>
    <String, dynamic>{
      'deviceConfigEchipSensorTransmitId':
          instance.deviceConfigEchipSensorTransmitId,
      'transmitRateId': instance.transmitRateId,
      'transmitRate': instance.transmitRate,
    };
