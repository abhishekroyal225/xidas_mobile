import 'dart:convert';

import 'package:ZettaWifiDirect/dashboard_screens/dashboard.dart';
import 'package:ZettaWifiDirect/login_screens/enter_otp.dart';
import 'package:ZettaWifiDirect/main_screens/WiFiDevices.dart';
import 'package:ZettaWifiDirect/services/auth.service.dart';
import 'package:ZettaWifiDirect/util/progressDialogBox.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class Login extends StatefulWidget {
  final String title;

  Login({Key key, this.title}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final String _loginURL = 'https://34.213.13.191:8443/user-login';
  double _height, _width;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool _fieldError = false;
  bool _isEmailFilled = false;
  bool _isPasswordFilled = false;
  bool _isPasswordSelected = false;
  bool _isEmailSelected = false;
  bool isUserIdFilled = false;
  bool _isHidden = true;
  bool _loading = false;

  String successMessage, _strEmail, _strPassword, userID = '';

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        color: Colors.white,
        margin: EdgeInsets.only(
          top: _height * 0.04,
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 47, right: 47, top: 60),
          child: ListView(
            children: <Widget>[
              Row(
                children: [
                  Image.asset(
                    'assets/images/High resolution preview for white BG (PNG) 4.png',
                    height: 62,
                  ),
                ],
              ),
              SizedBox(
                height: 64,
              ),
              Container(
                height: _height - 306,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        'Log in',
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 36,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(top: _height * 0.01),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Don't have an account?",
                            style: TextStyle(
                              color: Color(0xffb6b6b6),
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Text(
                            "  Sign up",
                            style: TextStyle(
                              color: Color(0xFF08F0A9),
                              fontSize: 14,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                      ),
                    ),
                    FocusScope(
                      child: Focus(
                        onFocusChange: (focus) => setState(() {
                          _isEmailSelected = focus;
                        }),
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          decoration: BoxDecoration(
                            // border: BorderDirectional(
                            //   start: BorderSide(
                            //     color: Color(0xFF08F0A9),
                            //     width: 2,
                            //   ),
                            // ),
                            borderRadius: new BorderRadius.all(
                                const Radius.circular(12.0)),
                            gradient: !_isEmailSelected
                                ? null
                                : new LinearGradient(
                                    stops: [0.02, 0.02],
                                    colors: [Color(0xFF08F0A9), Colors.white]),
                          ),
                          child: TextField(
                            controller: emailController,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.only(top: 24, bottom: 28),
                              prefixIcon: Icon(Icons.email_outlined),
                              floatingLabelBehavior: FloatingLabelBehavior.auto,
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                      width: 0.2, color: Colors.black54)),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                      width: 0.2, color: Colors.black54)),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12.0),
                                borderSide: BorderSide(
                                    width: 0.2, color: Colors.black54),
                              ),
                              labelText: 'Email',
                              // hintText: 'Enter Email ID',
                              errorText: _fieldError && !_isEmailFilled
                                  ? 'Email ID Cannot Be Empty'
                                  : null,
                              labelStyle: TextStyle(
                                  color: Color(0xFF838383),
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400),
                              helperStyle: TextStyle(
                                  color: Color(0xFF838383),
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400),
                              hintStyle: TextStyle(
                                  color: Color(0xFF838383),
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400),
                            ),
                            onChanged: (value) {
                              setState(() {
                                if (value == '' || value == null) {
                                  _isEmailFilled = false;
                                } else {
                                  _isEmailFilled = true;
                                }
                              });
                            },
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                    ),
                    FocusScope(
                      child: Focus(
                        onFocusChange: (focus) => setState(() {
                          _isPasswordSelected = focus;
                        }),
                        child: Container(
                          margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          decoration: BoxDecoration(
                            // border: BorderDirectional(
                            //   start: BorderSide(
                            //     color: Color(0xFF08F0A9),
                            //     width: 2,
                            //   ),
                            // ),
                            borderRadius: new BorderRadius.all(
                                const Radius.circular(12.0)),
                            gradient: !_isPasswordSelected
                                ? null
                                : new LinearGradient(
                                    stops: [0.02, 0.02],
                                    colors: [Color(0xFF08F0A9), Colors.white]),
                          ),
                          child: TextField(
                            obscureText: _isHidden,
                            controller: passwordController,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                  top: 24, bottom: 22, right: 10),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                      width: 0.2, color: Colors.black54)),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                      width: 0.2, color: Colors.black54)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(12.0),
                                  borderSide: BorderSide(
                                      width: 0.2, color: Colors.black54)),
                              prefixIcon: Icon(Icons.lock),
                              labelText: 'Password',
                              // hintText: 'Enter Password',
                              errorText: _fieldError && !_isPasswordFilled
                                  ? 'Password Cannot Be Empty'
                                  : null,
                              suffix: InkWell(
                                onTap: _togglePasswordView,
                                child: Icon(
                                  _isHidden
                                      ? Icons.visibility_off
                                      : Icons.visibility,
                                ),
                              ),
                              labelStyle: TextStyle(
                                  color: Color(0xFF838383),
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400),
                              helperStyle: TextStyle(
                                  color: Color(0xFF838383),
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400),
                              hintStyle: TextStyle(
                                  color: Color(0xFF838383),
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400),
                            ),
                            onChanged: (value) {
                              setState(() {
                                if (value == '' || value == null) {
                                  _isPasswordFilled = false;
                                } else {
                                  _isPasswordFilled = true;
                                }
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        TextButton(
                          child: Text(
                            'Forgot Password?',
                            style:
                                Theme.of(context).textTheme.bodyText2.copyWith(
                                      color: Color(0xff828282),
                                      fontWeight: FontWeight.w400,
                                    ),
                          ),
                          onPressed: () {
                            //forgot password screen
                          },
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 27,
                    ),
                    Container(
                      height: 60,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          primary: Colors.black, // background
                          onPrimary: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                        ),
                        child: Center(
                          child: Text(
                            'Sign in',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        onPressed: () {
                          if (_isEmailFilled && _isPasswordFilled) {
                            print(emailController.text);
                            print(passwordController.text);
                            _strEmail = emailController.text.trim();
                            _strPassword = passwordController.text.trim();
                            Provider.of<AuthService>(context, listen: false)
                                .setCredentials(email: _strEmail);
                            setState(() {
                              _fieldError = false;
                            });
                            _checkLoginCredentials(_strEmail, _strPassword);
                            // Navigator.pushAndRemoveUntil(
                            //     context,
                            //     CupertinoPageRoute(
                            //       builder: (context) =>
                            //           WiFiDevices(title: 'XIDAS IOT Mobile App'),
                            //     ),
                            //     (route) => true);
                          } else {
                            setState(() {
                              _fieldError = true;
                            });
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
              // Padding(
              //   padding: const EdgeInsets.all(8.0),
              //   child: Container(
              //     height: 50,
              //     child: ElevatedButton(
              //       style: ElevatedButton.styleFrom(
              //         primary: Colors.blue, // background
              //         onPrimary: Colors.white,
              //         shape: RoundedRectangleBorder(
              //           borderRadius: BorderRadius.circular(4.0),
              //         ),
              //       ),
              //       child: Text(
              //         'LOGIN USING OTP',
              //         style: TextStyle(fontSize: 18),
              //       ),
              //       onPressed: () => _openPopUp(context),
              //     ),
              //   ),
              // ),
              // Container(
              //   child: Row(
              //     children: <Widget>[
              //       Text('Dont Have An Account?'),
              //       FlatButton(
              //         textColor: Colors.blue,
              //         child: Text(
              //           'Sign in',
              //           style: TextStyle(fontSize: 20),
              //         ),
              //         onPressed: () {
              //           //signup screen
              //         },
              //       )
              //     ],
              //     mainAxisAlignment: MainAxisAlignment.center,
              //   ),
              // ),
              if (_loading) Center(child: CircularProgressIndicator()),
              SizedBox(height: 30),
              Image.asset(
                'assets/images/xidas_logo 2.png',
                height: 16,
              ),
              SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }

  void _togglePasswordView() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  _checkLoginCredentials(String email, String password) async {
    this._loading = true;
    final http.Response response = await http.post(
      Uri.parse(_loginURL),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'email': email,
        'password': password,
      }),
    );
    print('Response from _checkLoginCredentials ----  ' + response.body);
    this._loading = false;
    response.body.contains('Successfully logged in')
        ? Navigator.pushAndRemoveUntil(
            context,
            CupertinoPageRoute(
              builder: (context) => Dashboard(),
            ),
            (route) => true)
        : ProgressDialogBox.showPopUpDialog(
            context, 'title', 'User Not Registered');
  }

  snackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(text),
        duration: Duration(seconds: 2),
      ),
    );
  }

  _openPopUp(context) async {
    Alert(
        context: context,
        title: 'Generate OTP',
        content: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
          return Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  icon: Icon(Icons.email),
                  labelText: 'User ID',
                ),
                keyboardType: TextInputType.emailAddress,
                onChanged: (value) {
                  userID = value;
                  setState(() {
                    if (value == '' || value == null) {
                      isUserIdFilled = false;
                    } else {
                      isUserIdFilled = true;
                    }
                  });
                },
              ),
            ],
          );
        }),
        buttons: [
          DialogButton(
              child: Center(
                child: Text(
                  'GET OTP',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                  ),
                ),
              ),
              onPressed: () {
                if (isUserIdFilled) {
                  Navigator.pushAndRemoveUntil(
                      context,
                      CupertinoPageRoute(
                        builder: (context) =>
                            EnterOTP(title: 'XIDAS IOT Mobile App'),
                      ),
                      (route) => true);
                } else {
                  ProgressDialogBox.showPopUpDialog(
                      context, 'Warning', 'Please Enter User ID');
                }
              })
        ]).show();
  }
}
