import 'dart:io';

import 'package:ZettaWifiDirect/login_screens/view/login_screen.dart';
import 'package:ZettaWifiDirect/main_screens/Login.dart';
import 'package:ZettaWifiDirect/main_screens/WiFiDevices.dart';
import 'package:ZettaWifiDirect/services/auth.service.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

void main() {
  _enablePlatformOverrideForDesktop();
  HttpOverrides.global = CustomHttpOverride();
  runApp(MyApp());
}

void _enablePlatformOverrideForDesktop() {
  if (!kIsWeb && (Platform.isWindows || Platform.isLinux)) {
    debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  }
}

class CustomHttpOverride extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate certificate, String host, int port) => true;
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(375, 812),
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        // home: WifiDirect(title: 'XIDAS IOT Mobile App'),
        // home: WiFiDirectP2P(title: 'Zetta WiFi Direct P2P'),
        // home: WiFiDevices(title: 'XIDAS Mobile App'),
        home: ChangeNotifierProvider<AuthService>(
            create: (_) => AuthService(),
            builder: (context, snapshot) {
              return LoginScreen();
              //return Login(title: 'XIDAS IOT Mobile App');
            }),
      ),
    );
  }
}
