import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:ZettaWifiDirect/dashboard_screens/side_navigation_drawer.dart';
import 'package:ZettaWifiDirect/sensor_screens/DevicesList.dart';
import 'package:ZettaWifiDirect/sensor_screens/sensor_setup.dart';
import 'package:ZettaWifiDirect/services/auth.service.dart';
import 'package:ZettaWifiDirect/util/progressDialogBox.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wifi/wifi.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_aws_s3_client/flutter_aws_s3_client.dart';
import 'package:wifi_iot/wifi_iot.dart';

const String STA_DEFAULT_SSID = 'XIDAS-49_192.168.4.1';
const String STA_DEFAULT_PASSWORD = 'XIDAS@1234';
const NetworkSecurity STA_DEFAULT_SECURITY = NetworkSecurity.WPA;

class WiFiDevices extends StatefulWidget {
  final String title;
  WiFiDevices({this.title});

  @override
  _WiFiDevicesState createState() => _WiFiDevicesState();
}

class _WiFiDevicesState extends State<WiFiDevices> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String _wifiName = '  click button to get wifi ssid';
  int level = 0;
  String _ip = '  click button to get ip';
  List<WifiResult> ssidList = [];
  String ssid = '', password = '';
  TextEditingController _ipController = TextEditingController();

  String wifiJsonDataString = "";
  bool _isDevicesScanned = false;
  bool _isScanningDevices = false;
  bool isSsidFieldFilled = false;
  bool isIpFieldFilled = true;
  bool isPasswordFieldFilled = false;
  bool fieldError = false;
  bool _isConnected = false;
  bool _connectionStatus = false;
  String _connectedSSID = "";
  var wifiJsonData = {};
  Stream<double> _downloadState;
  String data;
  String provisioningStatus = 'NO';

  List<FileSystemEntity> files;
  String firmwareFile;

  List<String> _optionsAfterProvisioning = [
    'Device Configuration',
    'Firmware Upgrade'
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
        actions: <Widget>[
          Row(
            children: <Widget>[
              IconButton(
                  icon: Stack(
                    children: [
                      Icon(
                        Icons.notifications,
                        color: Colors.white,
                      ),
                    ],
                  ),
                  onPressed: () => ProgressDialogBox.showPopUpDialog(
                      context, 'Warning', 'Under Development'))
            ],
          )
        ],
      ),
      drawer: SideNavigationDrawer(_scaffoldKey.currentContext),
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.blue,
                      child: Text('SSID'),
                      onPressed: _getWifiName,
                    ),
                  ),
                  Offstage(
                    offstage: level == 0,
                    child: Image.asset(
                      level == 0
                          ? 'assets/images/wifi0.png'
                          : 'assets/images/wifi$level.png',
                      width: 28.0,
                      height: 21.0,
                    ),
                  ),
                  Text(
                    _wifiName,
                    style: TextStyle(
                        color: Colors.green[800],
                        fontSize: 18,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: RaisedButton(
                      textColor: Colors.white,
                      color: Colors.blue,
                      child: Text('IP'),
                      onPressed: _getIP,
                    ),
                  ),
                  Text(
                    _ip,
                    style: TextStyle(
                      color: Colors.green[800],
                      fontSize: 18,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: RaisedButton(
                          textColor: Colors.white,
                          color: Colors.blue,
                          child: Text(
                            'Scan Devices',
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                          onPressed: () {
                            loadData();
                            setState(() {
                              _isScanningDevices = true;
                            });
                          },
                        ),
                      ),
                      SizedBox(width: 8),
                      Container(
                        child: RaisedButton(
                          textColor: Colors.white,
                          color: Colors.blue,
                          child: Text(
                            'Get Sensor Devices',
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pushAndRemoveUntil(
                                context,
                                CupertinoPageRoute(
                                  builder: (context) => DevicesList(
                                      title: 'XIDAS IOT Mobile App'),
                                ),
                                (route) => true);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              _isScanningDevices
                  ? Expanded(
                      child: Center(
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.black,
                          valueColor:
                              new AlwaysStoppedAnimation<Color>(Colors.blue),
                        ),
                      ),
                    )
                  : _isDevicesScanned && ssidList.length > 0
                      ? Expanded(
                          child: ListView.builder(
                              padding: EdgeInsets.all(10.0),
                              itemCount: ssidList.length,
                              scrollDirection: Axis.vertical,
                              itemBuilder: (context, int index) {
                                return Card(
                                  shape: RoundedRectangleBorder(
                                    // side: new BorderSide(
                                    //     color: Colors.blue, width: 2.0),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  elevation: 3,
                                  child: Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        ListTile(
                                          leading: Image.asset(
                                              'assets/images/wifi${ssidList[index].level}.png',
                                              width: 28,
                                              height: 21),
                                          title: Text(
                                            ssidList[index].ssid,
                                            style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 16.0,
                                            ),
                                          ),
                                          trailing: _isConnected
                                              ? ssidList[index].ssid ==
                                                      STA_DEFAULT_SSID
                                                  ? Icon(Icons.circle,
                                                      color: Colors.green)
                                                  : SizedBox(
                                                      width: 1,
                                                    )
                                              : SizedBox(
                                                  width: 1,
                                                ),
                                          dense: true,
                                          onTap: () async {
                                            if (!_ip.contains('click')) {
                                              isSsidFieldFilled = false;
                                              isIpFieldFilled = true;
                                              isPasswordFieldFilled = false;
                                              fieldError = false;
                                              print(
                                                  '_connectedSSID: ${ssidList[index].ssid}');
                                              if (ssidList[index].ssid ==
                                                  STA_DEFAULT_SSID) {
                                                print(
                                                    'ssidList[index].ssid>>>> ${ssidList[index].ssid}');
                                                if (provisioningStatus ==
                                                    'NO') {
                                                  await WiFiForIoTPlugin.connect(
                                                      STA_DEFAULT_SSID,
                                                      password:
                                                          STA_DEFAULT_PASSWORD,
                                                      joinOnce: true,
                                                      security:
                                                          STA_DEFAULT_SECURITY);
                                                  await WiFiForIoTPlugin
                                                          .isConnected()
                                                      .then((value) {
                                                    _openPopUp(context);
                                                  });
                                                } else {
                                                  showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                          context) {
                                                        return AlertDialog(
                                                          title: Center(
                                                            child: Text(
                                                              'Choose Any Action',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .blueAccent),
                                                            ),
                                                          ),
                                                          content:
                                                              optionsAfterProvisioning(),
                                                        );
                                                      });
                                                }
                                              } else {
                                                _openPopUp(context);
                                              }
                                            } else {
                                              ProgressDialogBox.showPopUpDialog(
                                                  context,
                                                  'Warning',
                                                  'Please Get The Ip');
                                              print('Get The Ip');
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              }),
                        )
                      : SizedBox(
                          height: 0.1,
                        ),
              if (_downloadState != null)
                StreamBuilder(
                  stream: _downloadState,
                  builder: (_context, snap) {
                    if (snap.connectionState == ConnectionState.done) {
                      return Container();
                    }
                    if (snap.hasError) return Text(snap.error);
                    if (snap.connectionState == ConnectionState.active)
                      return Column(
                        children: [
                          Text(
                            '${(100 * snap.data)?.toInt()}%',
                            style: TextStyle(
                              color: Colors.blue,
                              fontSize: 16,
                            ),
                          ),
                          LinearProgressIndicator(
                            value: snap.data,
                          ),
                        ],
                      );
                    return Text('Nothing');
                  },
                ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _downloadState = _downloadAwsS3Files();
          });
        },
        tooltip: 'Download',
        child: Icon(Icons.file_download),
      ),
    );
  }

  void loadData() async {
    await Wifi.list('').then((list) {
      setState(() {
        ssidList = list;
        _isDevicesScanned = true;
        _isScanningDevices = false;
      });
    });
  }

  Future<Null> _getWifiName() async {
    int l = await Wifi.level;
    String wifiName = await Wifi.ssid;
    setState(() {
      level = l;
      _wifiName = wifiName;
    });
  }

  Future<Null> _getIP() async {
    String ip = await Wifi.ip;
    setState(() {
      _ip = ip;
      _ipController.text = _ip;
    });
  }

  Future<Null> connection() async {
    Wifi.connection(ssid, password).then((v) {
      print(v);
    });
  }

  snackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(text),
        duration: Duration(seconds: 2),
      ),
    );
  }

  _openPopUp(context) async {
    _getIP();
    Alert(
        context: context,
        title: 'WiFi Credentials',
        content: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
          return Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                    icon: Icon(Icons.wifi),
                    labelText: 'SSID',
                    errorText: fieldError && !isSsidFieldFilled
                        ? 'SSID Cannot Be Empty'
                        : null),
                keyboardType: TextInputType.text,
                onChanged: (value) {
                  ssid = value;
                  setState(() {
                    if (value == '' || value == null) {
                      isSsidFieldFilled = false;
                    } else {
                      isSsidFieldFilled = true;
                    }
                  });
                },
              ),
              TextField(
                decoration: InputDecoration(
                    icon: Icon(Icons.location_city_rounded),
                    labelText: 'IP ADDRESS',
                    errorText: fieldError && !isIpFieldFilled
                        ? 'IP Cannot Be Empty'
                        : null),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  _ip = value;
                  setState(() {
                    if (value == '' || value == null) {
                      isIpFieldFilled = false;
                    } else {
                      isIpFieldFilled = true;
                    }
                  });
                },
              ),
              TextField(
                obscureText: true,
                decoration: InputDecoration(
                  icon: Icon(Icons.lock),
                  labelText: 'Password',
                  errorText: fieldError && !isPasswordFieldFilled
                      ? 'Password Cannot Be Empty'
                      : null,
                  hintText: 'Enter Password',
                ),
                onChanged: (value) {
                  password = value;
                  setState(() {
                    if (value == '' || value == null) {
                      isPasswordFieldFilled = false;
                    } else {
                      isPasswordFieldFilled = true;
                    }
                  });
                },
              ),
            ],
          );
        }),
        buttons: [
          DialogButton(
              child: Center(
                child: Text(
                  'OK',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                  ),
                ),
              ),
              onPressed: () async {
                if (isPasswordFieldFilled && isSsidFieldFilled) {
                  //serializing json
                  wifiJsonData['SSID'] = ssid;
                  wifiJsonData['IP'] = _ip;
                  wifiJsonData['PASSWORD'] = password;
                  wifiJsonDataString = jsonEncode(wifiJsonData);
                  print('wifiJsonData [SSID]-->> ' + wifiJsonData['SSID']);
                  // print('wifiJsonData[IP]-->> ' + wifiJsonData['IP']);
                  print(
                      'wifiJsonData[PASSWORD]-->> ' + wifiJsonData['PASSWORD']);
                  print('Encoded JSON String-->> ' + wifiJsonDataString);
                  //decoding json
                  print('Decoded SSID-->> ' +
                      jsonDecode(wifiJsonDataString)['SSID']);
                  setState(() {
                    fieldError = false;
                    _connectedSSID = ssid;
                    ssid = "";
                    isPasswordFieldFilled = false;
                    isSsidFieldFilled = false;
                    _isConnected = true;
                  });
                  Navigator.pop(context);
                  snackBar('Encoded JSON---> ' + wifiJsonDataString);
                  snackBar('Decodec SSID---> ' +
                      jsonDecode(wifiJsonDataString)['SSID']);
                  ProgressDialogBox.showCircularProgressIndicator(
                      context, 'Checking wifi status');
                  final response = await getWifiStatus(wifiJsonData['SSID']);
                  Navigator.pop(context);
                  if (response < 0) {
                    print('Something is not right');
                    await Future.delayed(Duration(seconds: 0));
                    ProgressDialogBox.showPopUpDialog(
                        context, 'Wifi Status', 'Access Point Is Not Present');
                    return;
                  } else if (response > 5) {
                    print('Signal is dead');
                    await Future.delayed(Duration(seconds: 0));
                    ProgressDialogBox.showPopUpDialog(context, 'Wifi Status',
                        'Selected signal strength is low. connect to other network');
                    print('Failed');
                    return;
                  }
                  print('All is well');
                  final connect = await showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: Text(
                              'Do you want to connect',
                            ),
                            actions: [
                              TextButton(
                                child: Text('Connect'),
                                onPressed: () {
                                  Navigator.pop(context, true);
                                },
                              ),
                            ],
                          ));

                  if (!connect) return;
                  ProgressDialogBox.showCircularProgressIndicator(
                      context, 'Establishing Connection');
                  await postWiFiCredentials(
                    wifiJsonData['SSID'],
                    wifiJsonData['IP'],
                    wifiJsonData['PASSWORD'],
                  );
                } else {
                  Navigator.pop(context);
                  setState(() {
                    fieldError = true;
                  });
                  _openPopUp(context);
                }
              })
        ]).show();
  }

  Widget optionsAfterProvisioning() {
    return Container(
      height: 160.0,
      width: 200.0,
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: _optionsAfterProvisioning.length,
          itemBuilder: (context, int index) {
            return GestureDetector(
              onTap: () async {
                if (index == 0) {
                  Navigator.pushAndRemoveUntil(
                      context,
                      CupertinoPageRoute(
                        builder: (context) =>
                            DevicesList(title: 'XIDAS IOT Mobile App'),
                      ),
                      (route) => true);
                }
                if (index == 1) {
                  final extDir = await getExternalStorageDirectory();
                  final filePath = '${extDir.path}/xidas_firmwares/';
                  files = Directory(filePath).listSync();
                  for (var file in files) {
                    if (file.path.contains('.bin')) firmwareFile = file.path;
                    print('firmwareFile>>>>>$firmwareFile');
                  }
                  if (firmwareFile != null) {
                    Navigator.of(context).pop();
                    ProgressDialogBox.showCircularProgressIndicator(
                        context, 'Uploading the firmware');
                    int status = await uploadFirmwareToHost(firmwareFile);
                    if (status == 200) {
                      Navigator.of(context).pop();
                      ProgressDialogBox.showPopUpDialogWithOK(
                          context,
                          'Firmware Uploaded Successfully',
                          'Do You Want To Upgrade', () async {
                        Navigator.of(context).pop();
                        ProgressDialogBox.showCircularProgressIndicator(
                            context, 'Upgrading Firmware');
                        String statusString = await upgradeFirmware();
                        if (statusString == 'ACK') {
                          Navigator.of(context).pop();
                          ProgressDialogBox.showPopUpDialog(context, 'Status',
                              'Firmware Upgraded Successfully');

                          // TODO
                          // Hit API to get firmware Status
                          // we have to pass device id
                          final success =
                              await _getUpgradeDetailsAndSendToDMS();
                          if (success) {
                            Navigator.of(context).pop();
                            ProgressDialogBox.showPopUpDialog(
                                context, 'Upload Status', 'Success');
                          } else {
                            Navigator.of(context).pop();
                            ProgressDialogBox.showPopUpDialog(
                                context, 'Upload Status', 'Failed');
                          }
                        } else if (statusString == 'NACK') {
                          Navigator.of(context).pop();
                          ProgressDialogBox.showPopUpDialog(
                              context, 'Status', 'Firmware Upgrade Failed');
                        } else {
                          Navigator.of(context).pop();
                          ProgressDialogBox.showPopUpDialog(
                              context, 'Status', 'Something Went Wrong');
                        }
                      });
                    }
                  }

                  // showDialog(
                  //     context: context,
                  //     builder: (BuildContext context) {
                  //       return AlertDialog(
                  //         title: Center(
                  //           child: Text(
                  //             'Select File to Download',
                  //             style: TextStyle(color: Colors.blueAccent),
                  //           ),
                  //         ),
                  //         content: listOfFirmwares(files),
                  //       );
                  //     });
                }
              },
              child: Card(
                shape: RoundedRectangleBorder(
                  // side: new BorderSide(
                  //     color: Colors.blue, width: 2.0),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                elevation: 3,
                child: Padding(
                  padding: EdgeInsets.all(4.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        title: Text(
                          _optionsAfterProvisioning[index],
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }

  Widget listOfFirmwares(List<FileSystemEntity> firmwareFiles) {
    return Container(
      height: 220.0,
      width: 200.0,
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: firmwareFiles.length,
          itemBuilder: (context, int index) {
            return GestureDetector(
              onTap: () async {
                Navigator.of(context).pop();
                ProgressDialogBox.showCircularProgressIndicator(
                    context, 'Downloading Firmware');

                Future.delayed(Duration(seconds: 2), () async {
                  Navigator.of(_scaffoldKey.currentContext).pop();

                  ProgressDialogBox.showPopUpDialog(_scaffoldKey.currentContext,
                      'Download Status', 'Completed');

                  int status = await uploadFirmwareToHost('filePath');

                  if (status == 400) {
                    Navigator.of(_scaffoldKey.currentContext).pop();
                    ProgressDialogBox.showCircularProgressIndicator(
                        _scaffoldKey.currentContext,
                        'Firmware Uploaded Successfully. Rebooting..');

                    Future.delayed(Duration(seconds: 2), () {
                      Navigator.of(_scaffoldKey.currentContext).pop();

                      ProgressDialogBox.showPopUpDialog(
                          _scaffoldKey.currentContext,
                          'Reconnection Status',
                          'Successful');
                      // we will call rest api to store data to cloud
                      Navigator.of(_scaffoldKey.currentContext).pop();
                      ProgressDialogBox.showCircularProgressIndicator(
                          _scaffoldKey.currentContext, 'Uploading data to DMS');

                      Future.delayed(Duration(seconds: 2), () {
                        Navigator.of(_scaffoldKey.currentContext).pop();
                        ProgressDialogBox.showPopUpDialog(
                            _scaffoldKey.currentContext,
                            'Upload Status',
                            'Successful');
                      });
                    });
                  }
                });
              },
              child: Card(
                shape: RoundedRectangleBorder(
                  // side: new BorderSide(
                  //     color: Colors.blue, width: 2.0),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                elevation: 3,
                child: Padding(
                  padding: EdgeInsets.all(4.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTile(
                        title: Text(
                          firmwareFiles[index].path.split('/').last,
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }

  Future<int> uploadFirmwareToHost(String filePath) async {
    try {
      var fileName = getFileName(filePath);
      print('FileName>>>>>$fileName');
      var result = await http.post(
        Uri.parse('https://192.168.4.1:8888/upload/$fileName'),
        body: File(filePath).readAsBytesSync(),
      );
      return result.statusCode;
    } catch (e) {
      return 400; //handle exception
    }
  }

  Future<String> upgradeFirmware() async {
    String upgardeFirmwareURL = 'https://192.168.4.1:8888/echo?update=FWupdate';
    String responseString;
    var response = await http.post(
      Uri.parse(upgardeFirmwareURL),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
    if (response.statusCode == 200) {
      if (response.body.contains('ACK')) {
        responseString = 'ACK';
      } else {
        responseString = 'NACK';
      }
    } else {
      responseString = 'Something Went Wrong';
    }
    return responseString;
  }

  String getFileName(String filePath) {
    return filePath.split('/').last;
  }

  Future<int> getWifiStatus(String ssid) async {
    String url = 'https://192.168.4.1:8888/status?scan=$ssid';
    try {
      final response = await http.get(Uri.parse(url));
      print('Resp: ${response.body}');
      if (response.statusCode != 200) {
        print('Error ${response.statusCode}: ${response.body}');
        return -1;
      } else if (response.body.contains('is not present')) {
        return -2;
      }
      final responseJson = json.decode(response.body);
      return responseJson['rank'];
    } catch (e) {
      // throw Exception(e.toString());
      print('Exception>> ${e.toString()}');
      return 4;
    }
  }

  postWiFiCredentials(String ssid, String ip, String password) async {
    String _ipAddress = ip;
    String URL = 'https://' + _ipAddress + ':8888/echo?update=credentials';
    print('URL--->>>' + URL);
    snackBar('URL--->>>' + URL);
    try {
      final http.Response response = await http.post(
        Uri.parse(URL),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'ssid': ssid,
          'password': password,
        }),
      );
      print('response.body>>>' + response.body);
      print('Response from postWiFiCredentials ----  ' +
          response.statusCode.toString());
      Navigator.pop(context);
      if (response.body.contains('NACK')) {
        return await ProgressDialogBox.showPopUpDialog(
            context, 'Wifi Availability', 'No WiFI Available for Given SSID');
      } else {
        Navigator.of(context).pop();
        setState(() {
          provisioningStatus = 'YES';
        });
        return await ProgressDialogBox.showPopUpDialog(
            context, 'Provisioning Status', 'Complete');
      }
    } catch (e) {
      print('Exception>>> ${e.toString()}}');
      Navigator.of(context).pop();
      setState(() {
        provisioningStatus = 'YES';
      });
      return await ProgressDialogBox.showPopUpDialog(
          context, 'Provisioning Status', 'Complete');
    }
  }

  Stream<double> _downloadAwsS3Files() async* {
    const region = 'us-east-2';
    const bucketId = 'pcbpod-stepfiles';

    final AwsS3Client s3client = AwsS3Client(
        region: region,
        host: 's3.$region.amazonaws.com',
        bucketId: bucketId,
        accessKey: 'AKIAJGZDYSQTOH2MYUQQ',
        secretKey: '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');

    print('CLick');

    final extDir = await getExternalStorageDirectory();
    final filePath = '${extDir.path}/xidas_firmwares/XIDAS-IOTDEV.elf';

    File file = File(filePath);
    try {
      file.create(recursive: true);
    } catch (_) {}

    final signedParams = s3client.buildSignedGetParams(key: 'XIDAS-IOTDEV.elf');

    var httpClient = http.Client();
    var _request = new http.Request('GET', signedParams.uri);

    _request.headers.clear();
    _request.headers.addAll(signedParams.headers);
    var _response = httpClient.send(_request);

    print(_response);

    List<List<int>> chunks = new List();
    int downloaded = 0;
    int totalSize = 0;

    final responseStream = _response.asStream();

    await for (final http.StreamedResponse _response in responseStream) {
      if (_response.statusCode != HttpStatus.ok) {
        throw HttpException(
          'Error ${_response.statusCode}',
          uri: signedParams.uri,
        );
      }

      await for (List<int> chunk in _response.stream) {
        totalSize = _response.contentLength;
        chunks.add(chunk);
        downloaded += chunk.length;
        print('Downloaded: ${(downloaded / totalSize) * 100}');
        yield downloaded / totalSize;
      }

      final Uint8List bytes = Uint8List(totalSize.toInt());
      int offset = 0;
      for (List<int> chunk in chunks) {
        bytes.setRange(offset, offset + chunk.length, chunk);
        offset += chunk.length;
      }
      final _saved = await file.writeAsBytes(bytes);
      if (_saved.existsSync()) print('Saved to ${_saved.path}');
      // successDialog(context, 'Download Complete');
      Container();
      return;
    }
  }

  Future<bool> _getUpgradeDetailsAndSendToDMS() async {
    final deviceId = STA_DEFAULT_SSID.substring(
      0,
      STA_DEFAULT_SSID.indexOf('_'),
    );
    // Assumed API to get Upgrade details From DMS
    String url =
        'https://34.213.13.191:8080/get-upgrade-details-by-id?device_id=$deviceId';
    try {
      final response = await http.get(Uri.parse(url));
      print('Resp: ${response.body}');

      final Map<String, dynamic> upgradeDetails = json.decode(response.body);

      final email = Provider.of<AuthService>(context, listen: false).email;
      // Assumed API To save the details to DMS
      url = 'https://34.213.13.191:8080/save-upgrade-details-by-id';

      upgradeDetails['user_id'] = email;

      final updateResponse =
          await http.post(Uri.parse(url), body: json.encode(upgradeDetails));

      return updateResponse.statusCode == 200;
    } catch (e) {
      // throw Exception(e.toString());
      print('Exception>> ${e.toString()}');
      return false;
    }
  }
}
